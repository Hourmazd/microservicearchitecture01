﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using UiClient.ViewModels;

namespace UiClient.Data
{
    public class UiClientContext : DbContext
    {
        public UiClientContext (DbContextOptions<UiClientContext> options)
            : base(options)
        {
        }

        public DbSet<UiClient.ViewModels.ProductDTO> ProductDTO { get; set; }
    }
}
