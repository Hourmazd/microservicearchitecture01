﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using UiClient.ViewModels;

namespace UiClient.HttpClients
{
    public class ProductService
    {
        private readonly HttpClient _HttpClient;

        public ProductService(HttpClient httpClient)
        {
            _HttpClient = httpClient;
        }

        public async Task<IEnumerable<ProductDTO>> GetProducts()
        {
            //var response = await _HttpClient.GetAsync("https://localhost:5201/api/product/getall");
            var response = await _HttpClient.GetAsync("https://localhost:4001/api/product/getall");

            var str = await response.Content.ReadAsStringAsync();
            return !string.IsNullOrEmpty(str) ? JsonConvert.DeserializeObject<IEnumerable<ProductDTO>>(str) : new List<ProductDTO>();
        }

        public async Task<ProductDTO> GetProduct(int id)
        {
            //var response = await _HttpClient.GetAsync("https://localhost:5201/api/product/get/" + id);
            var response = await _HttpClient.GetAsync("https://localhost:4001/api/agg/product/get/" + id);

            if (response.IsSuccessStatusCode)
            {
                var str = await response.Content.ReadAsStringAsync();
                return !string.IsNullOrEmpty(str) ? JsonConvert.DeserializeObject<ProductDTO>(str) : null;
            }
            else
            {
                throw new Exception(response.ReasonPhrase);
            }
        }
    }
}
