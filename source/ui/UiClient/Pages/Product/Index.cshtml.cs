﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using UiClient.HttpClients;
using UiClient.ViewModels;

namespace UiClient.Pages.Product
{
    public class IndexModel : PageModel
    {
        private readonly ProductService _ProductService;

        public IndexModel(ProductService productService)
        {
            _ProductService = productService;
        }

        public IList<ProductDTO> ProductDTO { get;set; }

        public async Task OnGetAsync()
        {
            ProductDTO = (await _ProductService.GetProducts()).ToList();
        }
    }
}
