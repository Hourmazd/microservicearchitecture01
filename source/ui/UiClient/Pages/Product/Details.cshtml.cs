﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using UiClient.Data;
using UiClient.HttpClients;
using UiClient.ViewModels;

namespace UiClient.Pages.Product
{
    public class DetailsModel : PageModel
    {
        private readonly ProductService _ProductService;

        public DetailsModel(ProductService productService)
        {
            _ProductService = productService;
        }

        public ProductDTO ProductDTO { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            ProductDTO = await _ProductService.GetProduct(id.Value);

            if (ProductDTO == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
