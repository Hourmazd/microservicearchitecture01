﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using MSA.Common.API;
using Warehousing.Application.Commands.Warehouse;
using Warehousing.Application.Queries;
using Warehousing.Application.ViewModels;

namespace Warehousing.API.Controllers
{
    public class WarehouseController : BaseController<Domain.Entities.Warehouse, WarehouseDTO>
    {
        protected override IRequest<bool> DeleteCommand(int entityId)
        {
            return new WarehouseDeleteCommand() { Id = entityId };
        }

        protected override IRequest<IEnumerable<WarehouseDTO>> FetchListCommand()
        {
            return new WarehouseListQuery();
        }

        protected override IRequest<WarehouseDTO> FetchSingleCommand(int entityId)
        {
            return new WarehouseSingleQuery() { EntityId = entityId };
        }
    }
}