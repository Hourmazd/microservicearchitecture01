﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Warehousing.Domain.Entities;

namespace Warehousing.Persistence.Entity_Configuration
{
    public class WarehouseItemEntityTypeConfiguration : IEntityTypeConfiguration<WarehouseItem>
    {
        public void Configure(EntityTypeBuilder<WarehouseItem> builder)
        {
            builder.ToTable("WarehouseItem", WarehouseDbContext.DEFAULT_SCHEMA);
        }
    }
}
