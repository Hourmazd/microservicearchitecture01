﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Warehousing.Domain.Entities;

namespace Warehousing.Persistence.Entity_Configuration
{
    public class WarehouseEntityTypeConfiguration : IEntityTypeConfiguration<Warehouse>
    {
        public void Configure(EntityTypeBuilder<Warehouse> builder)
        {
            builder.ToTable("Warehouse", WarehouseDbContext.DEFAULT_SCHEMA);

            builder
                .HasMany(p => p.Items)
                .WithOne()
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
