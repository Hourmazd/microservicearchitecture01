﻿using Microsoft.EntityFrameworkCore;
using MSA.Common.Persistence;
using Warehousing.Domain.Entities;
using Warehousing.Persistence.Entity_Configuration;

namespace Warehousing.Persistence
{
    public class WarehouseDbContext : DbContextBase, IWarehouseDbContext
    {
        #region Constructors

        public WarehouseDbContext(DbContextOptions<WarehouseDbContext> options)
            : base(options)
        {
            System.Diagnostics.Debug.WriteLine($"WarehouseDbContext::ctor {GetHashCode()}");
        }

        #endregion

        #region Public Properties

        public const string DEFAULT_SCHEMA = "Warehousing";

        #endregion

        #region DbSets

        public DbSet<Warehouse> Warehouses { get; set; }

        #endregion

        #region Override Members

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new WarehouseEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new WarehouseItemEntityTypeConfiguration());
        }

        #endregion
    }
}
