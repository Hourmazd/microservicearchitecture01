﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using MSA.Common.Persistence;
using Warehousing.Domain.Entities;

namespace Warehousing.Persistence.Repositories
{
    public class WarehouseRepository : RepositoryBase<WarehouseDbContext, Warehouse>, IWarehouseRepository
    {
        public WarehouseRepository(WarehouseDbContext context, IMediator mediator)
            : base(context, mediator)
        {
        }

        protected override async Task LoadRelatedEntitiesOnSingleEntity(Warehouse entity)
        {
            await Context.Entry(entity).Collection(e => e.Items).LoadAsync();
        }

        public async Task<IEnumerable<Warehouse>> GetWarehouseWithSpecificProducts(int productId)
        {
            return await QueryableList.Where(e => e.Items.Any(i => i.ProductId == productId)).ToListAsync();
        }

        public async Task<int> GetWarehouseStockForSpecificProduct(int productId)
        {
            var item = await Context.Set<WarehouseItem>().FirstOrDefaultAsync(e => e.ProductId == productId);

            return item == null ? 0 : item.AvailableQuantity;
        }
    }
}
