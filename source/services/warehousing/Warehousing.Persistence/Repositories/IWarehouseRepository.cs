﻿using MSA.Common.Persistence;
using System.Collections.Generic;
using System.Threading.Tasks;
using Warehousing.Domain.Entities;

namespace Warehousing.Persistence.Repositories
{
    public interface IWarehouseRepository : IRepositoryBase<Warehouse>
    {
        Task<IEnumerable<Warehouse>> GetWarehouseWithSpecificProducts(int productId);
        Task<int> GetWarehouseStockForSpecificProduct(int productId);
    }
}
