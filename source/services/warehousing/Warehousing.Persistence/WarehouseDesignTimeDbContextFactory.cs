﻿using Microsoft.EntityFrameworkCore;
using MSA.Common.Persistence;

namespace Warehousing.Persistence
{
    public class WarehouseDesignTimeDbContextFactory : ContextDesignTimeDbContextFactoryBase<WarehouseDbContext>
    {
        protected override string ConnectionStringName => "SqlServer";
        protected override string ProjectName => "Warehousing.API";
        protected override string AspNetCoreEnvironment => "Development";

        protected override WarehouseDbContext CreateNewInstance(string connectionString)
        {
            var options = new DbContextOptionsBuilder<WarehouseDbContext>()
                .UseSqlServer(connectionString);

            return new WarehouseDbContext(options.Options);
        }
    }
}
