﻿using Microsoft.EntityFrameworkCore;
using MSA.Common.Persistence;
using Warehousing.Domain.Entities;

namespace Warehousing.Persistence
{
    public interface IWarehouseDbContext : IDbContextBase
    {
        DbSet<Warehouse> Warehouses { get; set; }
    }
}
