﻿using MSA.Common.Persistence;
using Warehousing.Domain.Entities;

namespace Warehousing.Application.Queries
{
    public class WarehouseDataFetchSpecification : DataFetchSpecification<Warehouse>
    {
        public WarehouseDataFetchSpecification()
            : base()
        {
            Includes.Add(e => e.Items);

            Sorts.Add(e => e.Code);
        }
    }
}
