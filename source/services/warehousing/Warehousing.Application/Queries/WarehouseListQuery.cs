﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.Extensions.Logging;
using Warehousing.Application.ViewModels;
using Warehousing.Persistence.Repositories;

namespace Warehousing.Application.Queries
{
    public class WarehouseListQuery : IRequest<IEnumerable<WarehouseDTO>>
    {
        public WarehouseListQuery()
        {
            Specification = new WarehouseDataFetchSpecification();
        }

        public WarehouseDataFetchSpecification Specification { get; }
    }

    public class WarehouseListQueryHandler : IRequestHandler<WarehouseListQuery, IEnumerable<WarehouseDTO>>
    {
        private readonly IWarehouseRepository _WarehouseRepository;
        private readonly ILogger<WarehouseListQueryHandler> _Logger;
        private readonly IMapper _Mapper;

        public WarehouseListQueryHandler(IWarehouseRepository repository, ILogger<WarehouseListQueryHandler> logger, IMapper mapper)
        {
            _WarehouseRepository = repository;
            _Logger = logger;
            _Mapper = mapper;
        }

        public async Task<IEnumerable<WarehouseDTO>> Handle(WarehouseListQuery request, CancellationToken cancellationToken)
        {
            var result = await _WarehouseRepository.GetList(request.Specification, cancellationToken);

            return _Mapper.Map<IEnumerable<WarehouseDTO>>(result);
        }
    }
}
