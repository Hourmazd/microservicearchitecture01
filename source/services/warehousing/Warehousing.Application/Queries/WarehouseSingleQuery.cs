﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using MediatR;
using System.Threading.Tasks;
using System.Threading;
using Warehousing.Persistence.Repositories;
using Warehousing.Application.ViewModels;

namespace Warehousing.Application.Queries
{
    public class WarehouseSingleQuery : IRequest<WarehouseDTO>
    {
        public int EntityId { get; set; }
    }

    public class WarehouseSingleQueryHandler : IRequestHandler<WarehouseSingleQuery, WarehouseDTO>
    {
        public IWarehouseRepository Repository { get; protected set; }
        public ILogger<WarehouseSingleQueryHandler> Logger { get; protected set; }
        public IMapper Mapper { get; protected set; }

        public WarehouseSingleQueryHandler(IWarehouseRepository repository, ILogger<WarehouseSingleQueryHandler> logger, IMapper mapper)
        {
            Repository = repository;
            Logger = logger;
            Mapper = mapper;
        }

        public async Task<WarehouseDTO> Handle(WarehouseSingleQuery request, CancellationToken cancellationToken)
        {
            var result = await Repository.GetEntityById(request.EntityId, cancellationToken);

            return Mapper.Map<WarehouseDTO>(result);
        }
    }
}
