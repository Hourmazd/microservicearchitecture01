﻿using MediatR;
using Microsoft.Azure.ServiceBus;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Text;
using System.Threading.Tasks;

namespace Warehousing.Application.EventBus
{
    public class IntegrationEventHandler
    {
        private readonly IMediator _Mediator;
        private readonly ILogger<IntegrationEventHandler> _Logger;

        public IntegrationEventHandler(IMediator mediator, ILogger<IntegrationEventHandler> logger)
        {
            _Mediator = mediator;
            _Logger = logger;
        }

        public async Task<bool> Handel(Message message, Type eventType)
        {
            var result = false;
            try
            {
                var messageData = Encoding.UTF8.GetString(message.Body);
                var @event = JsonConvert.DeserializeObject(messageData, eventType) as IRequest<bool>;
                result = await _Mediator.Send(@event, default);

                _Logger.LogInformation($"\t>>> Event {eventType} handeled sucessfully. (Id: {message.MessageId})");
            }
            catch(Exception ex)
            {
                _Logger.LogError(ex, $"\t>>> Error in handeling event {eventType}. (Id: {message.MessageId})");
            }

            return result;
        }
    }
}
