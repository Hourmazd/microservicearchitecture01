﻿using Microsoft.Azure.ServiceBus;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using MSA.Common.EventBus;
using System;
using System.Threading.Tasks;

namespace Warehousing.Application.EventBus
{
    public class WarehousingServiceBus : AzureServiceBus
    {
        private readonly IntegrationEventHandler _IntegrationEventHandler;

        public WarehousingServiceBus(
            IConfiguration configuration,
            ILogger<WarehousingServiceBus> logger,
            IServiceProvider serviceProvider)
            : base(configuration, logger)
        {
            var serviceScope = serviceProvider.CreateScope();
            _IntegrationEventHandler = serviceScope.ServiceProvider.GetService<IntegrationEventHandler>();
        }

        protected override async Task<bool> ProcessEvent(Message message)
        {
            var eventType = _SubscriptionTypes[message.Label];
            await _IntegrationEventHandler.Handel(message, eventType);

            return true;
        }
    }
}
