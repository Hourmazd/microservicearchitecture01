﻿using System.Collections.Generic;
using AutoMapper;
using MediatR;
using MSA.Common.Application;
using Warehousing.Domain.Entities;

namespace Warehousing.Application.ViewModels
{
    [AutoMap(typeof(Warehouse))]
    public class WarehouseDTO : DTOBase, IRequest<Warehouse>
    {
        public string Code { get; set; }

        public IEnumerable<WarehouseItemDTO> Items { get; set; }
    }
}
