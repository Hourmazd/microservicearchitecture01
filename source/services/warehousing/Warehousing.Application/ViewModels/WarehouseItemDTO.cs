﻿using AutoMapper;
using MSA.Common.Application;
using Warehousing.Domain.Entities;

namespace Warehousing.Application.ViewModels
{
    [AutoMap(typeof(WarehouseItem))]
    public class WarehouseItemDTO : DTOBase
    {
        public int ProductId { get; set; }

        public int StoreQuantity { get; set; }

        public int BookedQuantity { get; set; }

        public int AvailableQuantity { get; set; }
    }
}
