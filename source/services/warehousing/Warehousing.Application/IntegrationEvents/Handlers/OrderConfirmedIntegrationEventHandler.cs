﻿using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Warehousing.Application.IntegrationEvents.Events;
using Warehousing.Persistence.Repositories;

namespace Warehousing.Application.IntegrationEvents.Handlers
{
    public class OrderConfirmedChangedIntegrationEventHandler : IRequestHandler<OrderConfirmedIntegrationEvent, bool>
    {
        private readonly IWarehouseRepository _Repository;

        public OrderConfirmedChangedIntegrationEventHandler(IWarehouseRepository repository)
        {
            _Repository = repository ?? throw new ArgumentNullException(nameof(repository));
        }

        public async Task<bool> Handle(OrderConfirmedIntegrationEvent request, CancellationToken cancellationToken)
        {
            foreach (var product in request.OrderItems)
            {
                var warehoues = await _Repository.GetWarehouseWithSpecificProducts(product.Key);

                if (!warehoues.Any())
                    throw new Exception($"Unable to find product {product.Key} in warehouse.");

                var warehouseItems = warehoues.SelectMany(e => e.Items).Where(e => e.ProductId == product.Key);

                if (warehouseItems.Sum(e => e.AvailableQuantity) < product.Value)
                    throw new Exception($"Available quantity of product {product.Key} in warehouse is less than requested.");

                var remainQuantity = product.Value;

                do
                {
                    var item = warehouseItems.First(e => e.ProductId == product.Key);
                    var quantity = item.AvailableQuantity > product.Key ? product.Key : item.AvailableQuantity;
                    item.BookProduct(quantity);
                    remainQuantity -= quantity;
                }
                while (remainQuantity > 0);

                foreach (var warehouse in warehoues)
                    await _Repository.Update(warehouse);
            }

            return true;
        }
    }
}
