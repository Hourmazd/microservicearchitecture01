﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using Warehousing.Application.ViewModels;
using Warehousing.Persistence.Repositories;

namespace Warehousing.Application.Commands.Warehouse
{
    public class WarehouseUpsertCommandHandler : IRequestHandler<WarehouseDTO, Domain.Entities.Warehouse>
    {
        private readonly IMediator _Mediator;
        private readonly IWarehouseRepository _Repository;
        private readonly ILogger<WarehouseUpsertCommandHandler> _Logger;

        public WarehouseUpsertCommandHandler(IWarehouseRepository repository, IMediator mediator, ILogger<WarehouseUpsertCommandHandler> logger)
        {
            _Repository = repository;
            _Mediator = mediator;
            _Logger = logger;
        }

        public async Task<Domain.Entities.Warehouse> Handle(WarehouseDTO request, CancellationToken cancellationToken)
        {
            var entity = await _Repository.GetEntityById(request.Id, cancellationToken);

            if (entity == null)
            {
                entity = new Domain.Entities.Warehouse(request.Code);

                foreach (var item in request.Items)
                {
                    var warehouseItem = entity.Items.FirstOrDefault(e => e.ProductId == item.ProductId);

                    if (warehouseItem == null)
                    {
                        entity.AddItem(item.ProductId);
                        entity.AddProductQuantity(item.ProductId, item.StoreQuantity);
                    }
                }

                await _Repository.Add(entity);
            }
            else
            {
                entity.Update(request.Code);

                foreach (var item in request.Items)
                {
                    var warehouseItem = entity.Items.FirstOrDefault(e => e.ProductId == item.ProductId);

                    if (warehouseItem == null)
                    {
                        entity.AddItem(item.ProductId);
                        entity.AddProductQuantity(item.ProductId, item.StoreQuantity);
                    }
                }

                await _Repository.Update(entity);
            }

            return entity;
        }
    }
}
