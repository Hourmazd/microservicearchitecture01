﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using Warehousing.Persistence.Repositories;

namespace Warehousing.Application.Commands.Warehouse
{
    public class WarehouseDeleteCommand : IRequest<bool>
    {
        public int Id { get; set; }
    }

    public class WarehouseDeleteCommandHandler : IRequestHandler<WarehouseDeleteCommand, bool>
    {
        private readonly IMediator _Mediator;
        private readonly IWarehouseRepository _Repository;
        private readonly ILogger<WarehouseDeleteCommandHandler> _Logger;

        public WarehouseDeleteCommandHandler(IWarehouseRepository repository, IMediator mediator, ILogger<WarehouseDeleteCommandHandler> logger)
        {
            _Repository = repository;
            _Mediator = mediator;
            _Logger = logger;
        }

        public async Task<bool> Handle(WarehouseDeleteCommand request, CancellationToken cancellationToken)
        {
            var entity = await _Repository.GetEntityById(request.Id, cancellationToken);

            return await _Repository.Remove(entity);
        }
    }
}
