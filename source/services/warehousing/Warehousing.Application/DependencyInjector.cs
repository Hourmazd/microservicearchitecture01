﻿using System.Reflection;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.Azure.ServiceBus;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using MSA.Common.Application;
using MSA.Common.EventBus;
using Warehousing.Application.EventBus;
using Warehousing.Application.IntegrationEvents.Events;
using Warehousing.Persistence;

namespace Warehousing.Application
{
    public static class DependencyInjector
    {
        public static IServiceCollection AddApplication(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddMediatR(Assembly.GetExecutingAssembly());
            services.AddAutoMapper(Assembly.GetExecutingAssembly());
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(LoggingBehavior<,>));

            services.AddScoped<IntegrationEventHandler>();
            services.AddScoped<IntegrationEventPublisher>();

            var eventBusType = configuration.GetValue<string>("EventBus:Type");

            if (eventBusType == "Azure")
            {
                services.AddSingleton<IEventBus, WarehousingServiceBus>();
            }
            else if (eventBusType == "RabbitMQ")
            {
                //TODO: Implement specific RabbitMQ for this service
                services.AddSingleton<IEventBus, RabbitMQServiceBus>();
            }

            return services;
        }

        public static IApplicationBuilder AddIntegrationEventHandlers(this IApplicationBuilder app)
        {
            var eventBus = app.ApplicationServices.GetRequiredService<IEventBus>();

            eventBus.Subscribe<OrderConfirmedIntegrationEvent>();

            return app;
        }

        public static IServiceCollection AddCustomHealthCheck(this IServiceCollection services, IConfiguration configuration)
        {
            var hcBuilder = services.AddHealthChecks();

            hcBuilder.AddCheck("self", () => HealthCheckResult.Healthy());

            hcBuilder.AddDbContextCheck<WarehouseDbContext>();

            var setting = new AzureServiceBusSetting();
            configuration.GetSection("EventBus").Bind(setting);
            var csb = new ServiceBusConnectionStringBuilder(setting.Endpoint, setting.EntityPath, setting.SharedAccessKeyName, setting.SharedAccessKey);

            hcBuilder.AddAzureServiceBusTopic(
                        csb.GetNamespaceConnectionString(),
                        topicName: setting.EntityPath,
                        name: "azure-servicebus-check");

            return services;
        }
    }
}