﻿using MSA.Common.Domain;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Warehousing.Domain.Entities
{
    public class WarehouseItem : EntityBase
    {
        #region Constructors

        protected WarehouseItem() : base()
        {
        }

        public WarehouseItem(int productId, int storeQuantity, int bookedQuantity) : this()
        {
            ProductId = productId;
            StoreQuantity = storeQuantity;
            BookedQuantity = bookedQuantity;
        }

        #endregion

        #region Private Fields

        #endregion

        #region Properties

        [Required]
        public int ProductId { get; protected set; }

        [Required]
        public int StoreQuantity { get; protected set; }

        [Required]
        public int BookedQuantity { get; protected set; }

        [NotMapped] 
        public int AvailableQuantity => StoreQuantity - BookedQuantity;

        #endregion

        #region Public Methods

        public void AddQuantity(int quantity)
        {
            StoreQuantity += quantity;
        }

        public void BookProduct(int quantity)
        {
            if(quantity > StoreQuantity)
                throw new ArgumentException($"Product {ProductId} could not booked more than {AvailableQuantity}");

            BookedQuantity += quantity;
        }

        #endregion
    }
}
