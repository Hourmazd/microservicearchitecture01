﻿using MSA.Common.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Warehousing.Domain.Entities
{
    public class Warehouse : EntityBase
    {
        #region Constructors

        protected Warehouse() : base()
        {
            _Items = new List<WarehouseItem>();
        }

        public Warehouse(string code) : this()
        {
            Code = code;
        }

        #endregion

        #region Private 

        private readonly List<WarehouseItem> _Items;

        #endregion

        #region Properties

        [Required]
        public string Code { get; protected set; }

        public virtual IReadOnlyCollection<WarehouseItem> Items => _Items.AsReadOnly();

        #endregion

        #region Public Methods

        public void Update(string code)
        {
            if (code != null)
                Code = code;
        }

        public WarehouseItem AddItem(int productId)
        {
            var item = Items.FirstOrDefault(e => e.ProductId == productId);

            if (item == null)
            {
                item = new WarehouseItem(productId, 0, 0);
                _Items.Add(item);
            }

            return item;
        }

        public WarehouseItem AddProductQuantity(int productId, int quantity)
        {
            var item = AddItem(productId);

            item.AddQuantity(quantity);

            return item;
        }

        public void BookProduct(int productId, int quantity)
        {
            var item = Items.FirstOrDefault(e => e.ProductId == productId);

            if (item == null)
                throw new ArgumentException($"Unable to find product {productId} in warehouse {Id}");

            item.BookProduct(quantity);
        }

        #endregion
    }
}
