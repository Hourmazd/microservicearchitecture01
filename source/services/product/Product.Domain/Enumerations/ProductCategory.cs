﻿using MSA.Common.Types;

namespace Product.Domain.Enumerations
{
    public class ProductCategory : Enumeration
    {
        public static ProductCategory Library = new ProductCategory(1, nameof(Library));
        public static ProductCategory Kitchen = new ProductCategory(2, nameof(Kitchen));
        public static ProductCategory Glossary = new ProductCategory(3, nameof(Glossary));

        public ProductCategory() : base()
        {
        }

        public ProductCategory(int id, string name) : base(id, name)
        {
        }
    }
}
