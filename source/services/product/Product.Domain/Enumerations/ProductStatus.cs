﻿using MSA.Common.Types;

namespace Product.Domain.Enumerations
{
    public class ProductStatus : Enumeration
    {
        public static ProductStatus Active = new ProductStatus(1, nameof(Active));
        public static ProductStatus NotActive = new ProductStatus(2, nameof(NotActive));

        public ProductStatus() : base()
        {
        }

        public ProductStatus(int id, string name) : base(id, name)
        {
        }
    }
}
