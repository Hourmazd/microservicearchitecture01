﻿namespace Product.Domain.Events
{
    public class ProductStatusChangedDomainEvent : ProductDomainEventBase<Entities.Product>
    {
        public ProductStatusChangedDomainEvent(Entities.Product product) : base(product)
        {
        }
    }
}
