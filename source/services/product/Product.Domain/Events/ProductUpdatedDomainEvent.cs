﻿namespace Product.Domain.Events
{
    public class ProductUpdatedDomainEvent : ProductDomainEventBase<Entities.Product>
    {
        public ProductUpdatedDomainEvent(Entities.Product product) : base(product)
        {
        }
    }
}
