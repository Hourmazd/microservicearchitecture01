﻿using MSA.Common.Domain;

namespace Product.Domain.Events
{
    public abstract class ProductDomainEventBase<TEntity> : DomainEventBase
    where TEntity : EntityBase
    {
        protected ProductDomainEventBase(TEntity entity) : base(entity.Id)
        {
            Entity = entity;
        }

        public TEntity Entity { get; }

    }
}
