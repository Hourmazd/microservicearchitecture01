﻿namespace Product.Domain.Events
{
    public class ProductCreatedDomainEvent : ProductDomainEventBase<Entities.Product>
    {
        public ProductCreatedDomainEvent(Entities.Product product) : base(product)
        {
        }
    }
}
