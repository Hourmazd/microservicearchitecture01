﻿using MSA.Common.Domain;

namespace Product.Domain.Events
{
    public class ProductDeletedDomainEvent : DomainEventBase
    {
        public ProductDeletedDomainEvent(int entityId) : base(entityId)
        {
        }
    }
}
