﻿using MSA.Common.Domain;
using System.ComponentModel.DataAnnotations;
using Product.Domain.Events;
using Product.Domain.Enumerations;

namespace Product.Domain.Entities
{
    public class Product : EntityBase
    {
        #region Constructors

        protected Product() : base()
        {
        }

        public Product(int categoryId, string code, string title, decimal price) : this()
        {
            ProductStatusId = ProductStatus.Active.Id;

            if (IsTransient)
                AddDomainEvent(new ProductCreatedDomainEvent(this));

            Update(code, title, price, categoryId);
        }

        #endregion

        #region Private Fields
        #endregion

        #region Properties

        [Required]
        public string Code { get; protected set; }

        [Required]
        public string Title { get; protected set; }

        [Required]
        public decimal Price { get; protected set; }

        public int ProductCategoryId { get; protected set; }

        public ProductCategory Category { get; protected set; }

        public int ProductStatusId { get; protected set; }

        [Required]
        public ProductStatus Status { get; protected set; }

        #endregion

        #region Public Methods

        public void ChangeStatus(int newStatusId)
        {
            ProductStatusId = newStatusId;
            AddDomainEvent(new ProductStatusChangedDomainEvent(this));
        }

        public void Update(string code, string title, decimal price, int categoryId)
        {
            if (code != null)
                Code = code;

            if (title != null)
                Title = title;

            Price = price;

            ProductCategoryId = categoryId;

            if (!IsTransient)
                AddDomainEvent(new ProductUpdatedDomainEvent(this));
        }

        #endregion
    }
}
