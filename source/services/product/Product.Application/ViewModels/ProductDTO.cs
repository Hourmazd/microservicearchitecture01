﻿using AutoMapper;
using AutoMapper.Configuration.Annotations;
using MediatR;
using MSA.Common.Application;
using MSA.Common.Types;
using Product.Domain.Enumerations;

namespace Product.Application.ViewModels
{
    [AutoMap(typeof(Domain.Entities.Product))]
    public class ProductDTO : DTOBase, IRequest<Domain.Entities.Product>
    {
        public string Code { get; set; }

        public string Title { get; set; }

        public decimal Price { get; set; }

        [SourceMember(nameof(Domain.Entities.Product.ProductCategoryId))]
        public int CategoryId { get; set; }
        
        [SourceMember(nameof(Domain.Entities.Product.ProductStatusId))]
        public int StatusId { get; set; }

        public string CategoryName => Enumeration.FromValue<ProductCategory>(CategoryId).Name;
        
        public string StatusName => Enumeration.FromValue<ProductStatus>(CategoryId).Name;

        //[JsonIgnore]
        //[SourceMember(nameof(Domain.Entities.Product.Category))]
        //public ProductCategory Category { get; set; }

        //[JsonIgnore]
        //[SourceMember(nameof(Domain.Entities.Product.Status))]
        //public ProductStatus Status { get; set; }
    }
}
