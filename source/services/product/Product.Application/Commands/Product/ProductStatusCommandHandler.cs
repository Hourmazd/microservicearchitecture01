﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using Product.Persistence.Repositories;

namespace Product.Application.Commands.Product
{
    public class ProductStatusCommand : IRequest
    {
        public int ProductId { get; set; }
        public int StatusId { get; set; }
    }

    public class ProductStatusCommandHandler : IRequestHandler<ProductStatusCommand>
    {
        private readonly IProductRepository _Repository;
        private readonly ILogger<ProductStatusCommandHandler> _Logger;

        public ProductStatusCommandHandler(IProductRepository repository, ILogger<ProductStatusCommandHandler> logger)
        {
            _Repository = repository;
            _Logger = logger;
        }

        public async Task<Unit> Handle(ProductStatusCommand request, CancellationToken cancellationToken)
        {
            var order = await _Repository.GetEntityById(request.ProductId, cancellationToken);

            order.ChangeStatus(request.StatusId);
            await _Repository.Update(order, cancellationToken);

            return Unit.Value;
        }
    }
}
