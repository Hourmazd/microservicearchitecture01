﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using Product.Domain.Events;
using Product.Persistence.Repositories;

namespace Product.Application.Commands.Product
{
    public class ProductDeleteCommand : IRequest<bool>
    {
        public int Id { get; set; }
    }

    public class ProductDeleteCommandHandler : IRequestHandler<ProductDeleteCommand, bool>
    {
        private readonly IMediator _Mediator;
        private readonly IProductRepository _Repository;
        private readonly ILogger<ProductDeleteCommandHandler> _Logger;

        public ProductDeleteCommandHandler(IProductRepository repository, IMediator mediator, ILogger<ProductDeleteCommandHandler> logger)
        {
            _Repository = repository;
            _Mediator = mediator;
            _Logger = logger;
        }

        public async Task<bool> Handle(ProductDeleteCommand request, CancellationToken cancellationToken)
        {
            var entity = await _Repository.GetEntityById(request.Id);

            entity.AddDomainEvent(new ProductDeletedDomainEvent(request.Id));

            return await _Repository.Remove(entity, cancellationToken);
        }
    }
}
