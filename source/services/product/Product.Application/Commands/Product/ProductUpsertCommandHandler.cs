﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using Product.Application.EventBus;
using Product.Application.IntegrationEvents.Events;
using Product.Application.ViewModels;
using Product.Persistence.Repositories;

namespace Product.Application.Commands.Product
{
    public class ProductUpsertCommandHandler : IRequestHandler<ProductDTO, Domain.Entities.Product>
    {
        private readonly IMediator _Mediator;
        private readonly IProductRepository _Repository;
        private readonly ILogger<ProductUpsertCommandHandler> _Logger;
        private readonly IntegrationEventPublisher _IntegrationEventPublisher;

        public ProductUpsertCommandHandler(
            IProductRepository repository,
            IMediator mediator,
            ILogger<ProductUpsertCommandHandler> logger,
            IntegrationEventPublisher eventPublisher)
        {
            _Repository = repository;
            _Mediator = mediator;
            _Logger = logger;
            _IntegrationEventPublisher = eventPublisher;
        }

        public async Task<Domain.Entities.Product> Handle(ProductDTO request, CancellationToken cancellationToken)
        {
            var product = await _Repository.GetEntityById(request.Id, cancellationToken);

            var priceChanged = product != null && product.Price != request.Price;

            if (product == null)
            {
                product = new Domain.Entities.Product(request.CategoryId, request.Code, request.Title, request.Price);

                await _Repository.Add(product, cancellationToken);
            }
            else
            {
                product.Update(request.Code, request.Title, request.Price, request.CategoryId);
                await _Repository.Update(product, cancellationToken);
            }

            if (priceChanged)
            {
                var priceChangedEvent = new ProductPriceChangedIntegrationEvent(product.Id, product.Price, 0);

                await _IntegrationEventPublisher.PublishThroughEventBusAsync(priceChangedEvent);
            }

            return product;
        }
    }
}
