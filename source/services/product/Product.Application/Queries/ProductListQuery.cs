﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using Product.Application.ViewModels;
using Product.Persistence.Repositories;
using MediatR;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Threading;

namespace Product.Application.Queries
{
    public class ProductListQuery : IRequest<IEnumerable<ProductDTO>>
    {
        public ProductListQuery()
        {
            Specification = new ProductDataFetchSpecification();
        }
        
        public ProductDataFetchSpecification Specification { get; }
    }

    public class ProductListQueryHandler : IRequestHandler<ProductListQuery, IEnumerable<ProductDTO>>
    {
        public IProductRepository Repository { get; protected set; }
        public ILogger<ProductListQueryHandler> Logger { get; protected set; }
        public IMapper Mapper { get; protected set; }

        public ProductListQueryHandler(IProductRepository repository, ILogger<ProductListQueryHandler> logger, IMapper mapper)
        {
            Repository = repository;
            Logger = logger;
            Mapper = mapper;
        }

        public async Task<IEnumerable<ProductDTO>> Handle(ProductListQuery request, CancellationToken cancellationToken)
        {
            var result = await Repository.GetList(request.Specification, cancellationToken);

            return Mapper.Map<IEnumerable<ProductDTO>>(result);
        }
    }
}
