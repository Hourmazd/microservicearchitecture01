﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using Product.Application.ViewModels;
using Product.Persistence.Repositories;
using MediatR;
using System.Threading.Tasks;
using System.Threading;

namespace Product.Application.Queries
{
    public class ProductSingleQuery : IRequest<ProductDTO>
    {
        public int EntityId { get; set; }
    }

    public class ProductSingleQueryHandler : IRequestHandler<ProductSingleQuery, ProductDTO>
    {
        public IProductRepository Repository { get; protected set; }
        public ILogger<ProductSingleQueryHandler> Logger { get; protected set; }
        public IMapper Mapper { get; protected set; }

        public ProductSingleQueryHandler(IProductRepository repository, ILogger<ProductSingleQueryHandler> logger, IMapper mapper)
        {
            Repository = repository;
            Logger = logger;
            Mapper = mapper;
        }

        public async Task<ProductDTO> Handle(ProductSingleQuery request, CancellationToken cancellationToken)
        {
            var result = await Repository.GetEntityById(request.EntityId, cancellationToken);

            return Mapper.Map<ProductDTO>(result);
        }
    }
}
