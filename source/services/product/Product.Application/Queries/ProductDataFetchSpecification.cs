﻿using MSA.Common.Persistence;

namespace Product.Application.Queries
{
    public class ProductDataFetchSpecification : DataFetchSpecification<Domain.Entities.Product>
    {
        public ProductDataFetchSpecification() 
            : base()
        {
            Includes.Add(e => e.Category);
            Includes.Add(e => e.Status);

            Sorts.Add(e => e.Code);
        }
    }
}
