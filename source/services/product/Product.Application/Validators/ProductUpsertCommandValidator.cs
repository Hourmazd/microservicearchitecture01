﻿using FluentValidation;
using Product.Application.ViewModels;

namespace Product.Application.Validators
{
    public class ProductUpsertCommandValidator : AbstractValidator<ProductDTO>
    {
        public ProductUpsertCommandValidator()
        {
            RuleFor(e => e.Code)
                .NotEmpty()
                .NotNull()
                .WithMessage("Code could not be empty");
        }
    }
}
