﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using Product.Domain.Events;
using Product.Persistence.Repositories;

namespace Product.Application.DomainEventHandlers
{
    public class ProductStatusChangedDomainEventHandler : INotificationHandler<ProductStatusChangedDomainEvent>
    {
        private readonly ILogger<ProductStatusChangedDomainEventHandler> _Logger;
        private readonly IProductRepository _Repository;

        public ProductStatusChangedDomainEventHandler(
            ILogger<ProductStatusChangedDomainEventHandler> logger,
            IProductRepository repository)
        {
            _Logger = logger;
            _Repository = repository;
        }

        public Task Handle(ProductStatusChangedDomainEvent notification, CancellationToken cancellationToken)
        {
            _Logger.LogInformation($"\t>> Status of product {notification.EntityId} has changed.");

            return Task.CompletedTask;
        }
    }
}
