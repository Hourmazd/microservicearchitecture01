﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using Product.Domain.Events;
using Product.Persistence.Repositories;

namespace Product.Application.DomainEventHandlers
{
    public class ProductDeletedDomainEventHandler : INotificationHandler<ProductDeletedDomainEvent>
    {
        private readonly ILogger<ProductDeletedDomainEventHandler> _Logger;
        private readonly IProductRepository _Repository;

        public ProductDeletedDomainEventHandler(
            ILogger<ProductDeletedDomainEventHandler> logger,
            IProductRepository repository)
        {
            _Logger = logger;
            _Repository = repository;
        }

        public Task Handle(ProductDeletedDomainEvent notification, CancellationToken cancellationToken)
        {
            _Logger.LogInformation($"\t>> Product {notification.EntityId} has been deleted.");

            return Task.CompletedTask;
        }
    }
}
