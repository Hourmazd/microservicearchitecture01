﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using Product.Domain.Events;
using Product.Persistence.Repositories;

namespace Product.Application.DomainEventHandlers
{
    public class ProductUpdatedDomainEventHandler : INotificationHandler<ProductUpdatedDomainEvent>
    {
        private readonly ILogger<ProductUpdatedDomainEventHandler> _Logger;
        private readonly IProductRepository _Repository;

        public ProductUpdatedDomainEventHandler(
            ILogger<ProductUpdatedDomainEventHandler> logger,
            IProductRepository repository)
        {
            _Logger = logger;
            _Repository = repository;
        }

        public Task Handle(ProductUpdatedDomainEvent notification, CancellationToken cancellationToken)
        {
            _Logger.LogInformation($"\t>> Product {notification.EntityId} has updated.");

            return Task.CompletedTask;
        }
    }
}
