﻿using Microsoft.Extensions.Logging;
using MSA.Common.EventBus;
using System;
using System.Threading.Tasks;

namespace Product.Application.EventBus
{
    public class IntegrationEventPublisher
    {
        private readonly IEventBus _EventBus;
        private readonly ILogger<IntegrationEventPublisher> _Logger;

        public IntegrationEventPublisher(
            ILogger<IntegrationEventPublisher> logger,
            IEventBus eventBus)
        {
            _Logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _EventBus = eventBus ?? throw new ArgumentNullException(nameof(eventBus));
        }

        public async Task PublishThroughEventBusAsync(IntegrationEvent evt)
        {
            try
            {
                _Logger.LogInformation($"\t>>> Publishing integration event: {evt.GetType().Name} from Product Service - (event Id: {evt.Id})");

                await _EventBus.Publish(evt);
            }
            catch (Exception ex)
            {
                _Logger.LogError(ex, $"\t>>> ERROR Publishing integration event: {evt.GetType().Name} from Product Service - (event Id: {evt.Id})");
            }
        }
    }
}
