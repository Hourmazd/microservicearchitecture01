﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using Product.Persistence;

namespace Product.Application.SeedData
{
    public class DataSeedCommand : IRequest
    {
    }

    public class SeedDataCommandHandler : IRequestHandler<DataSeedCommand>
    {
        private readonly IProductDbContext _DbContext;
        private readonly ILogger<SeedDataCommandHandler> _Logger;

        public SeedDataCommandHandler(IProductDbContext dbContext, ILogger<SeedDataCommandHandler> logger)
        {
            _DbContext = dbContext;
            _Logger = logger;
        }

        public async Task<Unit> Handle(DataSeedCommand request, CancellationToken cancellationToken)
        {
            _Logger.LogInformation("\t>> Start DB common data seeding...");

            var seeder = new DataSeeder(_DbContext, _Logger);

           await seeder.SeedProductCategory(cancellationToken);
           await seeder.SeedProductStatus(cancellationToken);

           _Logger.LogInformation("\t>> DB common data seeding finished successfully.");

            return Unit.Value;
        }
    }
}
