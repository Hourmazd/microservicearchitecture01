﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MSA.Common.Types;
using Product.Domain.Enumerations;
using Product.Persistence;

namespace Product.Application.SeedData
{
    public class DataSeeder
    {
        private readonly IProductDbContext _ProductDbContext;
        private readonly ILogger _Logger;

        public DataSeeder(IProductDbContext dbContext, ILogger logger)
        {
            _ProductDbContext = dbContext;
            _Logger = logger;
        }

        public async Task SeedProductStatus(CancellationToken cancellationToken)
        {
            var data = Enumeration.GetItems<ProductStatus>();

            foreach (var item in data)
            {
                var dbItem = await _ProductDbContext.ProductStatuses.FindAsync(item.Id);

                if (dbItem == null)
                    _ProductDbContext.ProductStatuses.Add(item);
            }

            _Logger.LogInformation("\t>> Product status data seeding finished.");

            await _ProductDbContext.SaveChangesAsync(cancellationToken);
        }

        public async Task SeedProductCategory(CancellationToken cancellationToken)
        {
            var data = Enumeration.GetItems<ProductCategory>();

            foreach (var item in data)
            {
                var dbItem = await _ProductDbContext.ProductCategories.FindAsync(item.Id);

                if (dbItem == null)
                    _ProductDbContext.ProductCategories.Add(item);
            }

            _Logger.LogInformation("\t>> Product category data seeding finished.");

            await _ProductDbContext.SaveChangesAsync(cancellationToken);
        }
    }
}
