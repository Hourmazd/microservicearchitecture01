﻿using System.Collections.Generic;
using Product.Application.Commands.Product;
using Product.Application.Queries;
using Product.Application.ViewModels;
using System.Linq;
using MediatR;
using MSA.Common.API;
using Microsoft.AspNetCore.Authorization;

namespace Product.API.Controllers
{
    [Authorize]
    public class ProductController : BaseController<Domain.Entities.Product, ProductDTO>
    {
        protected override IRequest<IEnumerable<ProductDTO>> FetchListCommand()
        {
            var query = new ProductListQuery();

            query.Specification.SortExpression = e => e.OrderBy(o => o.Code);

            return query;
        }

        protected override IRequest<ProductDTO> FetchSingleCommand(int entityId)
        {
            return new ProductSingleQuery() { EntityId = entityId };
        }

        protected override IRequest<bool> DeleteCommand(int entityId)
        {
            return new ProductDeleteCommand { Id = entityId };
        }
    }
}