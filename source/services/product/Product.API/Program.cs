using System;
using System.Threading.Tasks;
using App.Metrics;
using App.Metrics.AspNetCore;
using App.Metrics.Filtering;
using App.Metrics.Formatters.Json;
using MediatR;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Product.Application.SeedData;
using Product.Persistence;
using Serilog;
using Serilog.Events;

namespace Product.API
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var host = CreateHostBuilder(args).Build();

            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;

                try
                {
                    var dbContext = services.GetRequiredService<ProductDbContext>();
                    await dbContext.Database.MigrateAsync();

                    var mediator = services.GetRequiredService<IMediator>();
                    await mediator.Send(new DataSeedCommand());
                }
                catch (Exception ex)
                {
                    var logger = scope.ServiceProvider.GetRequiredService<ILogger<Program>>();
                    logger.LogError(ex, "An error occurred while migrating or initializing the database.");
                }
            }

            host.Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration((context, config) =>
                {
                    var env = context.HostingEnvironment;

                    config.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                        .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true, reloadOnChange: true)
                        .AddJsonFile("appsettings.Local.json", optional: true, reloadOnChange: true);

                    config.AddEnvironmentVariables();
                })
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    var filter = new MetricsFilter().WhereType(MetricType.Timer);

                    webBuilder
                        .ConfigureMetricsWithDefaults((context, builder) =>
                        {
                            builder.Report.ToTextFile(options =>
                            {
                                options.MetricsOutputFormatter = new MetricsJsonOutputFormatter();
                                options.AppendMetricsToTextFile = true;
                                options.Filter = filter;
                                options.FlushInterval = TimeSpan.FromSeconds(20);
                                options.OutputPathAndFileName = @"e:\metrics.product.txt";
                            });
                        })
                        .UseSerilog((host, config) =>
                        {
                            config.WriteTo.Console();
                            config.WriteTo.File(@"e:/log.product.txt", LogEventLevel.Information);
                        })
                        .UseMetrics()
                        .UseStartup<Startup>();
                });
    }
}
