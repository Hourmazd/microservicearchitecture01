﻿using Microsoft.EntityFrameworkCore;
using MSA.Common.Persistence;

namespace Product.Persistence
{
    public class ProductDesignTimeDbContextFactory : ContextDesignTimeDbContextFactoryBase<ProductDbContext>
    {
        protected override string ConnectionStringName => "SqlServer";
        protected override string ProjectName => "Product.API";
        protected override string AspNetCoreEnvironment => "Development";

        protected override ProductDbContext CreateNewInstance(string connectionString)
        {
            var options = new DbContextOptionsBuilder<ProductDbContext>()
                .UseSqlServer(connectionString);

            return new ProductDbContext(options.Options);
        }
    }
}
