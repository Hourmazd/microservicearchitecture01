﻿using MSA.Common.Persistence;
using Microsoft.EntityFrameworkCore;
using Product.Domain.Enumerations;

namespace Product.Persistence
{
    public interface IProductDbContext : IDbContextBase
    {
        DbSet<Domain.Entities.Product> Products { get; set; }

        DbSet<ProductStatus> ProductStatuses { get; set; }

        DbSet<ProductCategory> ProductCategories { get; set; }
    }
}
