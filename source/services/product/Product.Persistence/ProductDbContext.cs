﻿using Microsoft.EntityFrameworkCore;
using MSA.Common.Persistence;
using Product.Domain.Enumerations;
using Product.Persistence.Entity_Configuration;

namespace Product.Persistence
{
    public class ProductDbContext : DbContextBase, IProductDbContext
    {
        #region Constructors

        public ProductDbContext(DbContextOptions<ProductDbContext> options)
            : base(options)
        {
            System.Diagnostics.Debug.WriteLine($"ProductDbContext::ctor {GetHashCode()}");
        }

        #endregion

        #region Public Properties

        public const string DEFAULT_SCHEMA = "Products";

        #endregion

        #region DbSets

        public DbSet<Domain.Entities.Product> Products { get; set; }

        public DbSet<ProductStatus> ProductStatuses { get; set; }

        public DbSet<ProductCategory> ProductCategories { get; set; }

        #endregion

        #region Override Members

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new ProductEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new ProductCategoryEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new ProductStatusEntityTypeConfiguration());
        }

        #endregion
    }
}
