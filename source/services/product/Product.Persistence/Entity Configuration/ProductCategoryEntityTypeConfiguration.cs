﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Product.Domain.Enumerations;

namespace Product.Persistence.Entity_Configuration
{
    public class ProductCategoryEntityTypeConfiguration : IEntityTypeConfiguration<ProductCategory>
    {
        public void Configure(EntityTypeBuilder<ProductCategory> builder)
        {
            builder.ToTable("ProductCategory", ProductDbContext.DEFAULT_SCHEMA);

            builder.Property(e => e.Id)
                .ValueGeneratedNever()
                .IsRequired();

            builder.Property(e => e.Name)
                .HasMaxLength(20)
                .IsRequired();
        }
    }
}
