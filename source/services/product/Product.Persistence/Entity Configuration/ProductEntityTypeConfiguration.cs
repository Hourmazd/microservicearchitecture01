﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Product.Persistence.Entity_Configuration
{
    public class ProductEntityTypeConfiguration : IEntityTypeConfiguration<Domain.Entities.Product>
    {
        public void Configure(EntityTypeBuilder<Domain.Entities.Product> builder)
        {
            builder.ToTable("Product", ProductDbContext.DEFAULT_SCHEMA);

            builder.Property(p => p.Price)
                .HasColumnType("decimal(10,2)")
                .IsRequired();
        }
    }
}
