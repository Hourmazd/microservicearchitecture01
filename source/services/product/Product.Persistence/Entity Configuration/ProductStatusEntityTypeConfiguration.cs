﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Product.Domain.Enumerations;

namespace Product.Persistence.Entity_Configuration
{
    public class ProductStatusEntityTypeConfiguration : IEntityTypeConfiguration<ProductStatus>
    {
        public void Configure(EntityTypeBuilder<ProductStatus> builder)
        {
            builder.ToTable("ProductStatus", ProductDbContext.DEFAULT_SCHEMA);

            builder.Property(e => e.Id)
                .ValueGeneratedNever()
                .IsRequired();

            builder.Property(e => e.Name)
                .HasMaxLength(20)
                .IsRequired();
        }
    }
}
