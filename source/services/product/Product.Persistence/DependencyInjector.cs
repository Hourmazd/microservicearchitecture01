﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Product.Persistence.Repositories;

namespace Product.Persistence
{
    public static class DependencyInjector
    {
        public static IServiceCollection AddPersistence(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<ProductDbContext>(options =>
            {
                options.UseSqlServer(configuration.GetConnectionString("SqlServer"));
            });

            services.AddScoped<IProductDbContext>(provider => provider.GetService<ProductDbContext>());

            services.AddScoped<IProductRepository, ProductRepository>();

            return services;
        }
    }
}