﻿using MSA.Common.Persistence;

namespace Product.Persistence.Repositories
{
    public interface IProductRepository : IRepositoryBase<Domain.Entities.Product>
    {
    }
}
