﻿using System.Threading.Tasks;
using MediatR;
using MSA.Common.Persistence;

namespace Product.Persistence.Repositories
{
    public class ProductRepository : RepositoryBase<ProductDbContext, Domain.Entities.Product>, IProductRepository
    {
        public ProductRepository(ProductDbContext context, IMediator mediator)
            : base(context, mediator)
        {
        }

        protected override async Task LoadRelatedEntitiesOnSingleEntity(Domain.Entities.Product entity)
        {
            await Context.Entry(entity).Reference(e => e.Category).LoadAsync();
            await Context.Entry(entity).Reference(e => e.Status).LoadAsync();
        }
    }
}
