﻿using MSA.Common.Persistence;
using Ordering.Domain.Entities;

namespace Ordering.Application.Queries
{
    public class OrderDataFetchSpecification : DataFetchSpecification<Order>
    {
        public OrderDataFetchSpecification() 
            : base()
        {
            Includes.Add(e => e.OrderItems);
            Includes.Add(e => e.Status);
        }
    }
}
