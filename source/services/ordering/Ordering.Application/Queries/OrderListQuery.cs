﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.Extensions.Logging;
using Ordering.Application.ViewModels;
using Ordering.Persistence.Repositories;

namespace Ordering.Application.Queries
{
    public class OrderListQuery : IRequest<IEnumerable<OrderDTO>>
    {
        public OrderListQuery()
        {
            Specification = new OrderDataFetchSpecification();
        }

        public OrderDataFetchSpecification Specification { get; }
    }

    public class OrderListQueryHandler : IRequestHandler<OrderListQuery, IEnumerable<OrderDTO>>
    {
        private readonly IOrderRepository _OrderRepository;
        private readonly ILogger<OrderListQueryHandler> _Logger;
        private readonly IMapper _Mapper;

        public OrderListQueryHandler(IOrderRepository orderRepository, ILogger<OrderListQueryHandler> logger, IMapper mapper)
        {
            _OrderRepository = orderRepository;
            _Logger = logger;
            _Mapper = mapper;
        }

        public async Task<IEnumerable<OrderDTO>> Handle(OrderListQuery request, CancellationToken cancellationToken)
        {
            var result = await _OrderRepository.GetList(request.Specification, cancellationToken);

            return _Mapper.Map<IEnumerable<OrderDTO>>(result);
        }
    }
}
