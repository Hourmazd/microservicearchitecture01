﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using Ordering.Application.ViewModels;
using Ordering.Persistence.Repositories;
using MediatR;
using System.Threading.Tasks;
using System.Threading;

namespace Ordering.Application.Queries
{
    public class OrderSingleQuery : IRequest<OrderDTO>
    {
        public int EntityId { get; set; }
    }

    public class OrderSingleQueryHandler : IRequestHandler<OrderSingleQuery, OrderDTO>
    {
        public IOrderRepository Repository { get; protected set; }
        public ILogger<OrderSingleQueryHandler> Logger { get; protected set; }
        public IMapper Mapper { get; protected set; }

        public OrderSingleQueryHandler(IOrderRepository repository, ILogger<OrderSingleQueryHandler> logger, IMapper mapper)
        {
            Repository = repository;
            Logger = logger;
            Mapper = mapper;
        }

        public async Task<OrderDTO> Handle(OrderSingleQuery request, CancellationToken cancellationToken)
        {
            var result = await Repository.GetEntityById(request.EntityId, cancellationToken);

            return Mapper.Map<OrderDTO>(result);
        }
    }
}
