﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using Ordering.Application.IntegrationEvents.Events;
using Ordering.Persistence.Repositories;

namespace Ordering.Application.IntegrationEvents.Handlers
{
    public class ProductPriceChangedIntegrationEventHandler : IRequestHandler<ProductPriceChangedIntegrationEvent, bool>
    {
        private readonly ILogger<ProductPriceChangedIntegrationEventHandler> _Logger;
        private readonly IOrderRepository _Repository;

        public ProductPriceChangedIntegrationEventHandler(
            ILogger<ProductPriceChangedIntegrationEventHandler> logger, IOrderRepository repository)
        {
            _Logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _Repository = repository ?? throw new ArgumentNullException(nameof(repository));
        }

        public async Task<bool> Handle(ProductPriceChangedIntegrationEvent request, CancellationToken cancellationToken)
        {
            var orders = await _Repository.GetOpenOrdersOfSpecificProduct(request.ProductId);

            foreach (var order in orders)
            {
                var items = order.OrderItems.Where(e => e.ProductId == request.ProductId);

                foreach (var item in items)
                {
                    item.SetPrice(request.NewPrice);
                }

               await _Repository.Update(order);
            }

            return true;
        }
    }
}
