﻿using MSA.Common.EventBus;
using MediatR;

namespace Ordering.Application.IntegrationEvents.Events
{
    public class ProductPriceChangedIntegrationEvent : IntegrationEvent, IRequest<bool>
    {
        public int ProductId { get; }

        public decimal NewPrice { get; }

        public decimal OldPrice { get; }

        public ProductPriceChangedIntegrationEvent(int productId, decimal newPrice, decimal oldPrice)
        {
            ProductId = productId;
            NewPrice = newPrice;
            OldPrice = oldPrice;
        }
    }
}
