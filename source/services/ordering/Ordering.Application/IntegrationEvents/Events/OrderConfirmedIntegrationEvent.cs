﻿using MSA.Common.EventBus;
using MediatR;
using System.Collections.Generic;

namespace Ordering.Application.IntegrationEvents.Events
{
    public class OrderConfirmedIntegrationEvent : IntegrationEvent, IRequest<bool>
    {
        public IDictionary<int, int> OrderItems { get; set; }

        public OrderConfirmedIntegrationEvent()
        {
            OrderItems = new Dictionary<int, int>();
        }
    }
}
