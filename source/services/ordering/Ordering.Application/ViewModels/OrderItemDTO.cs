﻿using AutoMapper;
using MSA.Common.Application;
using Ordering.Domain.Entities;

namespace Ordering.Application.ViewModels
{
    [AutoMap(typeof(OrderItem))]
    public class OrderItemDTO : DTOBase
    {
        public int ProductId { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
    }
}
