﻿using System;
using System.Collections.Generic;
using AutoMapper;
using AutoMapper.Configuration.Annotations;
using MediatR;
using MSA.Common.Application;
using MSA.Common.Types;
using Ordering.Domain.Entities;
using Ordering.Domain.Enumerations;

namespace Ordering.Application.ViewModels
{
    [AutoMap(typeof(Order))]
    public class OrderDTO : DTOBase, IRequest<Order>
    {
        public DateTime OrderDate { get; set; }

        [SourceMember(nameof(Order.OrderStatusId))]
        public int StatusId { get; set; }

        public string StatusName => Enumeration.FromValue<OrderStatus>(StatusId).Name;
        
        public IEnumerable<OrderItemDTO> OrderItems { get; set; }
    }
}
