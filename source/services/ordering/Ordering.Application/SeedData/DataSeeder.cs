﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MSA.Common.Types;
using Ordering.Domain.Enumerations;
using Ordering.Persistence;

namespace Ordering.Application.SeedData
{
    public class DataSeeder
    {
        private readonly IOrderDbContext _OrderDbContext;
        private readonly ILogger _Logger;

        public DataSeeder(IOrderDbContext orderDbContext, ILogger logger)
        {
            _OrderDbContext = orderDbContext;
            _Logger = logger;
        }

        public async Task SeedOrderStatus(CancellationToken cancellationToken)
        {
            var data = Enumeration.GetItems<OrderStatus>();

            foreach (var item in data)
            {
                var dbItem = await _OrderDbContext.OrderStatus.FindAsync(item.Id);

                if (dbItem == null)
                    _OrderDbContext.OrderStatus.Add(item);
            }

            _Logger.LogInformation("\t\t>> Order status data seeding finished.");

            await _OrderDbContext.SaveChangesAsync(cancellationToken);
        }
    }
}
