﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using Ordering.Persistence;

namespace Ordering.Application.SeedData
{
    public class DataSeedCommand : IRequest
    {
    }

    public class SeedDataCommandHandler : IRequestHandler<DataSeedCommand>
    {
        private readonly IOrderDbContext _OrderDbContext;
        private readonly ILogger<SeedDataCommandHandler> _Logger;

        public SeedDataCommandHandler(IOrderDbContext orderDbContext, ILogger<SeedDataCommandHandler> logger)
        {
            _OrderDbContext = orderDbContext;
            _Logger = logger;
        }

        public async Task<Unit> Handle(DataSeedCommand request, CancellationToken cancellationToken)
        {
            _Logger.LogInformation("\t>> Start DB common data seeding...");

            var seeder = new DataSeeder(_OrderDbContext, _Logger);

           await seeder.SeedOrderStatus(cancellationToken);

           _Logger.LogInformation("\t>> DB common data seeding finished successfully.");

            return Unit.Value;
        }
    }
}
