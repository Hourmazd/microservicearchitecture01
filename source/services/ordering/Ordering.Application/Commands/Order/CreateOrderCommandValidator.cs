﻿using System.Linq;
using FluentValidation;
using Microsoft.Extensions.Logging;
using Ordering.Application.ViewModels;

namespace Ordering.Application.Commands.Order
{
    public class CreateOrderCommandValidator : AbstractValidator<OrderDTO>
    {
        public CreateOrderCommandValidator(ILogger<CreateOrderCommandValidator> logger)
        {
            RuleFor(command => command.StatusId).Must(e => e > 0);
            RuleFor(command => command.OrderItems).Must(items => items.Any()).WithMessage("No order items found");
        }
    }
}
