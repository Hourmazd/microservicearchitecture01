﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using Ordering.Application.ViewModels;
using Ordering.Domain.Events;
using Ordering.Persistence.Repositories;

namespace Ordering.Application.Commands.Order
{
    public class OrderUpsertCommandHandler : IRequestHandler<OrderDTO, Domain.Entities.Order>
    {
        private readonly IMediator _Mediator;
        private readonly IOrderRepository _OrderRepository;
        private readonly ILogger<OrderUpsertCommandHandler> _Logger;

        public OrderUpsertCommandHandler(
            IOrderRepository orderRepository, 
            IMediator mediator, 
            ILogger<OrderUpsertCommandHandler> logger)
        {
            _OrderRepository = orderRepository;
            _Mediator = mediator;
            _Logger = logger;
        }

        public async Task<Domain.Entities.Order> Handle(OrderDTO request, CancellationToken cancellationToken)
        {
            var order = await _OrderRepository.GetEntityById(request.Id, cancellationToken);

            if (order == null)
            {
                order = new Domain.Entities.Order(request.OrderDate);

                order.AddDomainEvent(new OrderCreatedDomainEvent(order));

                foreach (var item in request.OrderItems)
                    order.UpsertOrderItem(item.ProductId, item.Quantity, item.Price);

                await _OrderRepository.Add(order);
            }
            else
            {
                order.SetOrderDate(request.OrderDate);

                foreach (var item in request.OrderItems)
                    order.UpsertOrderItem(item.ProductId, item.Quantity, item.Price);

                await _OrderRepository.Update(order);
            }

            return order;
        }
    }
}
