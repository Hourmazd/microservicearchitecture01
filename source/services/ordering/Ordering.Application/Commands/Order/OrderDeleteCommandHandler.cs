﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using Ordering.Domain.Events;
using Ordering.Persistence.Repositories;

namespace Ordering.Application.Commands.Order
{
    public class OrderDeleteCommand : IRequest<bool>
    {
        public int Id { get; set; }
    }

    public class OrderDeleteCommandHandler : IRequestHandler<OrderDeleteCommand, bool>
    {
        private readonly IMediator _Mediator;
        private readonly IOrderRepository _OrderRepository;
        private readonly ILogger<OrderDeleteCommandHandler> _Logger;

        public OrderDeleteCommandHandler(IOrderRepository orderRepository, IMediator mediator, ILogger<OrderDeleteCommandHandler> logger)
        {
            _OrderRepository = orderRepository;
            _Mediator = mediator;
            _Logger = logger;
        }

        public async Task<bool> Handle(OrderDeleteCommand request, CancellationToken cancellationToken)
        {
            var entity = await _OrderRepository.GetEntityById(request.Id);

            entity.AddDomainEvent(new OrderDeletedDomainEvent(entity));

           return await _OrderRepository.Remove(entity);
        }
    }
}
