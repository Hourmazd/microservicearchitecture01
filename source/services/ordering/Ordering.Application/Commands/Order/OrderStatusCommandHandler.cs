﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using Ordering.Application.IntegrationEvents.Events;
using Ordering.Persistence.Repositories;
using Ordering.Application.EventBus;

namespace Ordering.Application.Commands.Order
{
    public class OrderStatusCommand : IRequest
    {
        public int OrderId { get; set; }
        public int StatusId { get; set; }
    }

    public class OrderStatusCommandHandler : IRequestHandler<OrderStatusCommand>
    {
        private readonly IOrderRepository _OrderRepository;
        private readonly ILogger<OrderStatusCommandHandler> _Logger;
        private readonly IntegrationEventPublisher _IntegrationEventPublisher;

        public OrderStatusCommandHandler(
            IOrderRepository orderRepository, 
            ILogger<OrderStatusCommandHandler> logger,
            IntegrationEventPublisher integrationEventPublisher)
        {
            _OrderRepository = orderRepository;
            _Logger = logger;
            _IntegrationEventPublisher = integrationEventPublisher;
        }

        public async Task<Unit> Handle(OrderStatusCommand request, CancellationToken cancellationToken)
        {
            var order = await _OrderRepository.GetEntityById(request.OrderId, cancellationToken);

            if (order.OrderStatusId == request.StatusId)
                return Unit.Value;

            order.SetOrderStatus(request.StatusId);
            await _OrderRepository.Update(order, cancellationToken);

            var items = new Dictionary<int, int>();

            foreach (var item in order.OrderItems)
                items.Add(item.ProductId, item.Quantity);

            var integrationEvent = new OrderConfirmedIntegrationEvent() { OrderItems = items };
            await _IntegrationEventPublisher.PublishThroughEventBusAsync(integrationEvent);

            return Unit.Value;
        }
    }
}
