﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using Ordering.Domain.Events;
using Ordering.Persistence.Repositories;

namespace Ordering.Application.DomainEventHandlers
{
    public class OrderCreatedDomainEventHandler : INotificationHandler<OrderCreatedDomainEvent>
    {
        private readonly ILoggerFactory _LoggerFactory;
        private readonly IOrderRepository _OrderRepository;

        public OrderCreatedDomainEventHandler(
            ILoggerFactory loggerFactory,
            IOrderRepository orderRepository)
        {
            _LoggerFactory = loggerFactory;
            _OrderRepository = orderRepository;
        }

        public Task Handle(OrderCreatedDomainEvent notification, CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }
}
