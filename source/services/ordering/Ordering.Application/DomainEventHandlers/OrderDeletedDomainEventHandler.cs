﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using Ordering.Domain.Events;
using Ordering.Persistence.Repositories;

namespace Ordering.Application.DomainEventHandlers
{
    public class OrderDeletedDomainEventHandler : INotificationHandler<OrderDeletedDomainEvent>
    {
        private readonly ILoggerFactory _LoggerFactory;
        private readonly IOrderRepository _OrderRepository;

        public OrderDeletedDomainEventHandler(
            ILoggerFactory loggerFactory,
            IOrderRepository orderRepository)
        {
            _LoggerFactory = loggerFactory;
            _OrderRepository = orderRepository;
        }

        public Task Handle(OrderDeletedDomainEvent notification, CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }
}
