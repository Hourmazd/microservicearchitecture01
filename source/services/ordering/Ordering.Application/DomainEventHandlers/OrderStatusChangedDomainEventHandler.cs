﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using Ordering.Domain.Events;
using Ordering.Persistence.Repositories;

namespace Ordering.Application.DomainEventHandlers
{
    public class OrderStatusChangedDomainEventHandler : INotificationHandler<OrderStatusChangedDomainEvent>
    {
        private readonly ILoggerFactory _LoggerFactory;
        private readonly IOrderRepository _OrderRepository;

        public OrderStatusChangedDomainEventHandler(
            ILoggerFactory loggerFactory,
            IOrderRepository orderRepository)
        {
            _LoggerFactory = loggerFactory;
            _OrderRepository = orderRepository;
        }

        public Task Handle(OrderStatusChangedDomainEvent notification, CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }
}
