﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using MSA.Common.API;
using Ordering.Application.Commands.Order;
using Ordering.Application.Queries;
using Ordering.Application.ViewModels;
using Ordering.Domain.Entities;
using Ordering.Domain.Enumerations;

namespace Ordering.API.Controllers
{
    public class OrderController : BaseController<Order, OrderDTO>
    {
        protected override IRequest<bool> DeleteCommand(int entityId)
        {
            return new OrderDeleteCommand() { Id = entityId };
        }

        protected override IRequest<IEnumerable<OrderDTO>> FetchListCommand()
        {
            return new OrderListQuery();
        }

        protected override IRequest<OrderDTO> FetchSingleCommand(int entityId)
        {
            return new OrderSingleQuery() { EntityId = entityId };
        }

        [Route("{orderId:int}/Confirm")]
        [HttpPost]
        public async Task<ActionResult<OrderDTO>> ConfirmOrder(int orderId, CancellationToken cancellationToken)
        {
            await Mediator.Send(new OrderStatusCommand() { OrderId = orderId, StatusId = OrderStatus.StockConfirmed.Id }, cancellationToken);

            return await Get(orderId, cancellationToken);
        }
    }
}