﻿using System.ComponentModel.DataAnnotations;
using System.IO;
using MSA.Common.Domain;

namespace Ordering.Domain.Entities
{
    public class OrderItem : EntityBase
    {
        #region Constructors

        public OrderItem() : base() { }

        public OrderItem(int productId, int quantity, decimal price) : this()
        {
            ProductId = productId;
            SetQuantity(quantity);
            SetPrice(price);
        }

        #endregion

        #region Public Properties

        [Required]
        public int ProductId { get; protected set; }

        [Required]
        public int Quantity { get; protected set; }

        [Required]
        [DataType("decimal(10, 2)")]
        [Range(0.0, double.MaxValue)]
        public decimal Price { get; protected set; }

        #endregion

        #region Public Methods

        public void SetPrice(decimal price)
        {
            if (price < 0)
                throw new InvalidDataException("Price could not be less than 0.");

            Price = price;
        }

        public void SetQuantity(int quantity)
        {
            if (quantity < 0)
                throw new InvalidDataException("Quantity could not be less than 0.");

            Quantity = quantity;
        }

        #endregion
    }
}
