﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using MSA.Common.Domain;
using Ordering.Domain.Enumerations;
using Ordering.Domain.Events;

namespace Ordering.Domain.Entities
{
    public class Order : EntityBase, IAggregate
    {
        #region Constructors

        protected Order() : base()
        {
            _OrderItems = new List<OrderItem>();
        }

        public Order(DateTime orderDate) : this()
        {
            SetOrderDate(orderDate);
            SetOrderStatus(OrderStatus.Submitted.Id);
        }

        #endregion

        #region Private Fields

        private readonly List<OrderItem> _OrderItems;

        #endregion

        #region Properties

        [Required]
        public DateTime OrderDate { get; protected set; }

        public int OrderStatusId { get; protected set; }

        public OrderStatus Status { get; protected set; }
        
        public virtual IReadOnlyCollection<OrderItem> OrderItems => _OrderItems.AsReadOnly();

        #endregion

        #region Public Methods

        public void UpsertOrderItem(int productId, int quantity, decimal price)
        {
            var item = _OrderItems.FirstOrDefault(e => e.ProductId == productId);

            if (item != default)
            {
                item.SetPrice(price);
                item.SetQuantity(quantity);
            }
            else
            {
                item = new OrderItem(productId, quantity, price);
                _OrderItems.Add(item);
            }
        }

        public void ChangeOrderItemQuantity(int productId, int quantity)
        {
            var item = _OrderItems.FirstOrDefault(e => e.ProductId == productId);

            if (item != default)
            {
                item.SetQuantity(quantity);
            }
            else
            {
                throw new KeyNotFoundException($"Unable to find orderItem for book Id: {productId}");
            }
        }

        public void SetOrderDate(DateTime date)
        {
            if(date <= DateTime.Today)
                throw new ArgumentException("Order date could not be less than today.");

            if(!IsTransient)
                AddDomainEvent(new OrderUpdatedDomainEvent(this));

            OrderDate = date;
        }

        public void SetOrderStatus(int newStatusId)
        {
            OrderStatusId = newStatusId;

            if (!IsTransient)
            {
                AddDomainEvent(new OrderUpdatedDomainEvent(this));
                AddDomainEvent(new OrderStatusChangedDomainEvent(this));
            }
        }

        #endregion
    }
}
