﻿using Ordering.Domain.Entities;

namespace Ordering.Domain.Events
{
    public class OrderStatusChangedDomainEvent : OrderDomainEventBase<Order>
    {
        public OrderStatusChangedDomainEvent(Order order) : base(order)
        {
        }
    }
}
