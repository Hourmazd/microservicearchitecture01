﻿using Ordering.Domain.Entities;

namespace Ordering.Domain.Events
{
    public class OrderCreatedDomainEvent : OrderDomainEventBase<Order>
    {
        public OrderCreatedDomainEvent(Order entity) : base(entity)
        {
        }
    }
}
