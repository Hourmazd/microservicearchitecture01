﻿using MSA.Common.Domain;

namespace Ordering.Domain.Events
{
    public abstract class OrderDomainEventBase<TEntity> : DomainEventBase
    where TEntity : EntityBase
    {
        protected OrderDomainEventBase(TEntity entity) : base(entity.Id)
        {
            Entity = entity;
        }

        public TEntity Entity { get; set; }
    }
}
