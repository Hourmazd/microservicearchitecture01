﻿using Ordering.Domain.Entities;

namespace Ordering.Domain.Events
{
    public class OrderUpdatedDomainEvent : OrderDomainEventBase<Order>
    {
        public OrderUpdatedDomainEvent(Order entity) : base(entity)
        {
        }
    }
}
