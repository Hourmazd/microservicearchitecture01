﻿using Ordering.Domain.Entities;

namespace Ordering.Domain.Events
{
    public class OrderDeletedDomainEvent : OrderDomainEventBase<Order>
    {
        public OrderDeletedDomainEvent(Order entity) : base(entity)
        {
        }
    }
}
