﻿using MSA.Common.Types;

namespace Ordering.Domain.Enumerations
{
    public class OrderStatus : Enumeration
    {
        public static OrderStatus Submitted = new OrderStatus(1, nameof(Submitted));
        public static OrderStatus StockValidation = new OrderStatus(2, nameof(StockValidation));
        public static OrderStatus StockConfirmed = new OrderStatus(3, nameof(StockConfirmed));
        public static OrderStatus Paid = new OrderStatus(4, nameof(Paid));
        public static OrderStatus Shipped = new OrderStatus(5, nameof(Shipped));
        public static OrderStatus Cancelled = new OrderStatus(6, nameof(Cancelled));

        public OrderStatus(int id, string name) : base(id, name)
        {
        }

        //public IEnumerable<Order> Orders { get; set; }
    }
}
