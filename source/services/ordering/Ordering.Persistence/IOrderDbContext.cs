﻿using Microsoft.EntityFrameworkCore;
using MSA.Common.Persistence;
using Ordering.Domain.Entities;
using Ordering.Domain.Enumerations;

namespace Ordering.Persistence
{
    public interface IOrderDbContext : IDbContextBase
    {
        DbSet<Order> Orders { get; set; }
        DbSet<OrderStatus> OrderStatus { get; set; }
    }
}
