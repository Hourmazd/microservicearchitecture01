﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Ordering.Persistence.Repositories;

namespace Ordering.Persistence
{
    public static class DependencyInjector
    {
        public static IServiceCollection AddPersistence(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<OrderDbContext>(options =>
            {
                options.UseSqlServer(configuration.GetConnectionString("SqlServer"));
            });

            services.AddScoped<IOrderDbContext>(provider => provider.GetService<OrderDbContext>());

            services.AddScoped<IOrderRepository, OrderRepository>();

            return services;
        }
    }
}