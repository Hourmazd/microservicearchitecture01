﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Ordering.Domain.Enumerations;

namespace Ordering.Persistence.Entity_Configuration
{
    public class OrderStatusEntityTypeConfiguration : IEntityTypeConfiguration<OrderStatus>
    {
        public void Configure(EntityTypeBuilder<OrderStatus> builder)
        {
            builder.ToTable("OrderStatus", OrderDbContext.DEFAULT_SCHEMA);

            builder.HasKey(e => e.Id);

            builder.Property(e => e.Id)
                .ValueGeneratedNever()
                .IsRequired();

            builder.Property(e => e.Name)
                .HasMaxLength(20)
                .IsRequired();
        }
    }
}
