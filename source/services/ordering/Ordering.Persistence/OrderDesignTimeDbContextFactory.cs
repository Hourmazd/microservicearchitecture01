﻿using Microsoft.EntityFrameworkCore;
using MSA.Common.Persistence;

namespace Ordering.Persistence
{
    public class OrderDesignTimeDbContextFactory : ContextDesignTimeDbContextFactoryBase<OrderDbContext>
    {
        protected override string ConnectionStringName => "SqlServer";
        protected override string ProjectName => "Ordering.API";
        protected override string AspNetCoreEnvironment => "Development";

        protected override OrderDbContext CreateNewInstance(string connectionString)
        {
            var options = new DbContextOptionsBuilder<OrderDbContext>()
                .UseSqlServer(connectionString);

            return new OrderDbContext(options.Options);
        }
    }
}
