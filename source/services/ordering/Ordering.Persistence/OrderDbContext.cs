﻿using Microsoft.EntityFrameworkCore;
using MSA.Common.Persistence;
using Ordering.Domain.Entities;
using Ordering.Persistence.Entity_Configuration;
using Ordering.Domain.Enumerations;

namespace Ordering.Persistence
{
    public class OrderDbContext : DbContextBase, IOrderDbContext
    {
        #region Constructors

        public OrderDbContext(DbContextOptions<OrderDbContext> options)
        : base(options)
        {
            System.Diagnostics.Debug.WriteLine($"OrderContext::ctor {GetHashCode()}");
        }

        #endregion

        #region Public Properties

        public const string DEFAULT_SCHEMA = "OrderSchema";

        #endregion

        #region DbSets

        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderStatus> OrderStatus { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }

        #endregion

        #region Override Members

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new OrderEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new OrderStatusEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new OrderItemEntityTypeConfiguration());
        }

        #endregion
    }
}
