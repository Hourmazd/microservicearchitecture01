﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using MSA.Common.Persistence;
using Ordering.Domain.Entities;
using Ordering.Domain.Enumerations;

namespace Ordering.Persistence.Repositories
{
    public class OrderRepository : RepositoryBase<OrderDbContext, Order>, IOrderRepository
    {
        public OrderRepository(OrderDbContext context, IMediator mediator)
            : base(context, mediator)
        {
        }

        protected override async Task LoadRelatedEntitiesOnSingleEntity(Order entity)
        {
            await Context.Entry(entity).Collection(e => e.OrderItems).LoadAsync();
            await Context.Entry(entity).Reference(e => e.Status).LoadAsync();
        }

        public async Task<IEnumerable<Order>> GetOpenOrdersOfSpecificProduct(int productId)
        {
            return await QueryableList
                .Include(e => e.OrderItems)
                .Where(e =>
                    e.OrderStatusId == OrderStatus.Submitted.Id &&
                    e.OrderItems.Any(i => i.ProductId == productId)).ToListAsync();
        }
    }
}
