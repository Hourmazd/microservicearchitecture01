﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MSA.Common.Persistence;
using Ordering.Domain.Entities;

namespace Ordering.Persistence.Repositories
{
    public interface IOrderRepository : IRepositoryBase<Order>
    {
        Task<IEnumerable<Order>> GetOpenOrdersOfSpecificProduct(int productId);
    }
}
