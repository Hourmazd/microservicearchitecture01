﻿using IdentityModel;
using IdentityServer4;
using IdentityServer4.Models;
using IdentityServer4.Test;
using System.Collections.Generic;
using System.Security.Claims;

namespace IdentityServer.Application.Configuration
{
    public class IdentityConfiguration
    {
        public static IEnumerable<IdentityResource> GetIdentityResources()
        {
            return new List<IdentityResource>
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile()
            };
        }

        // ApiResources define the apis in your system
        public static IEnumerable<ApiResource> GetApiResources()
        {
            return new List<ApiResource>
            {
                new ApiResource{
                    Name = "ProductAPI",
                    DisplayName = "Product API Service",
                    Scopes = new List<Scope> {
                        new Scope("ProductAPI")
                    }
                },
                new ApiResource{
                    Name = "OrderAPI",
                    DisplayName = "Order API Service",
                    Scopes = new List<Scope> {
                        new Scope("OrderAPI")
                    }
                },
                new ApiResource{
                    Name = "WarehouseAPI",
                    DisplayName = "Warehouse API Service",
                    Scopes = new List<Scope> {
                        new Scope("WarehouseAPI")
                    }
                },
                new ApiResource{
                    Name = "AggregatorAPI",
                    DisplayName = "Aggregator API Service",
                    Scopes = new List<Scope> {
                        new Scope("AggregatorAPI")
                    }
                }
            };
        }

        // client want to access resources (aka scopes)
        public static IEnumerable<Client> GetClients(Dictionary<string, string> clientsUrl)
        {
            return new List<Client>
            {
                new Client
                {
                    ClientId = "ApiGateway",
                    ClientName = "Credentials Authentication for API Gateway",
                    ClientSecrets = new List<Secret>
                    {
                        new Secret("secret".Sha256())
                    },
                    AllowedGrantTypes = GrantTypes.ClientCredentials,
                    AllowedScopes = new List<string>
                    {
                        "ProductAPI",
                        "OrderAPI",
                        "WarehouseAPI",
                        "AggregatorAPI"
                    },
                    AccessTokenLifetime = 60*60*2, // 2 hours
                    IdentityTokenLifetime= 60*60*2 // 2 hours
                },
                new Client
                {
                    ClientId = "UiClient",
                    ClientName = "UI Client",
                    ClientSecrets = new List<Secret>
                    {
                        new Secret("secret".Sha256())
                    },
                    ClientUri = $"{clientsUrl["UiClient"]}",           // public uri of the client
                    AllowedGrantTypes = GrantTypes.Hybrid,
                    AllowAccessTokensViaBrowser = true,
                    RequireConsent = false,
                    AllowOfflineAccess = true,
                    AlwaysIncludeUserClaimsInIdToken = true,
                    RedirectUris = new List<string>
                    {
                        $"{clientsUrl["UiClient"]}/signin-oidc"
                    },
                    PostLogoutRedirectUris = new List<string>
                    {
                        $"{clientsUrl["UiClient"]}/signout-callback-oidc"
                    },
                    AllowedScopes = new List<string>
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        IdentityServerConstants.StandardScopes.OfflineAccess,
                        "ProductAPI",
                        "OrderAPI",
                        "WarehouseAPI",
                        "AggregatorAPI"
                    },
                    AccessTokenLifetime = 60*60*2, // 2 hours
                    IdentityTokenLifetime= 60*60*2 // 2 hours
                },
            };
        }

        public static IEnumerable<TestUser> TestUsers =>
            new TestUser[]
            {
                new TestUser {
                    SubjectId = "5BE86359-073C-434B-AD2D-A3932222DABE",
                    Username = "bob",
                    Password = "123",
                    Claims = new List<Claim> {
                        new Claim(JwtClaimTypes.Email, "scott@scottbrady91.com"),
                        new Claim(JwtClaimTypes.Role, "admin")
                    }
                }
            };
    }
}

