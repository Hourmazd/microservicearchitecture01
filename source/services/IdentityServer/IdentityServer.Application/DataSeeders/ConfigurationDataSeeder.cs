﻿using IdentityServer.Application.Configuration;
using IdentityServer4.EntityFramework.DbContexts;
using IdentityServer4.EntityFramework.Mappers;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IdentityServer.Application.DataSeeders
{
    public class ConfigurationDataSeeder
    {
        public async Task SeedAsync(
            ConfigurationDbContext context,
            IConfiguration configuration,
            ILogger<ConfigurationDataSeeder> logger)
        {

            //callbacks urls from config:
            var clientUrls = new Dictionary<string, string>
            {
                { "ProductApi", configuration.GetValue<string>("Urls:ProductApiClient") },
                { "UiClient", configuration.GetValue<string>("Urls:UiClient") }
            };

            try
            {
                logger.LogInformation("Seeding data {DbContextName} ...", nameof(ConfigurationDataSeeder));

                foreach (var client in IdentityConfiguration.GetClients(clientUrls))
                {
                    var entity = await context.Clients.FirstOrDefaultAsync(e => e.ClientId == client.ClientId);

                    if (entity != null)
                        context.Clients.Remove(entity);

                    context.Clients.Add(client.ToEntity());
                }
                await context.SaveChangesAsync();

                foreach (var resource in IdentityConfiguration.GetIdentityResources())
                {
                    var entity = await context.IdentityResources.FirstOrDefaultAsync(e => e.Name == resource.Name);

                    if (entity != null)
                        context.IdentityResources.Remove(entity);

                    context.IdentityResources.Add(resource.ToEntity());
                }
                await context.SaveChangesAsync();

                foreach (var api in IdentityConfiguration.GetApiResources())
                {
                    var entity = await context.ApiResources.FirstOrDefaultAsync(e => e.Name == api.Name);

                    if (entity != null)
                        context.ApiResources.Remove(entity);

                    context.ApiResources.Add(api.ToEntity());
                }
                await context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "Error while seeding data {DbContextName}", nameof(ConfigurationDbContext));
            }
        }
    }
}
