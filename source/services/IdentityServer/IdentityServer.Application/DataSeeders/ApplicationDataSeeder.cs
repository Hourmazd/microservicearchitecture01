﻿using IdentityServer.Domain.Entities;
using IdentityServer.Persistence;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityServer.Application.DataSeeders
{
    public class ApplicationDataSeeder
    {
        private readonly IPasswordHasher<ApplicationUser> _passwordHasher = new PasswordHasher<ApplicationUser>();

        public async Task SeedAsync(
            ApplicationDbContext context,
            ILogger<ApplicationDataSeeder> logger)
        {
            try
            {
                if (!context.Users.Any())
                {
                    logger.LogInformation("Seeding data {DbContextName} ...", nameof(ApplicationDbContext));

                    context.Users.AddRange(GetDefaultUser());

                    await context.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "Error while seeding data {DbContextName}", nameof(ApplicationDbContext));
            }
        }

        private IEnumerable<ApplicationUser> GetDefaultUser()
        {
            var user =
            new ApplicationUser()
            {
                CardHolderName = "DemoUser",
                CardNumber = "4012888888881881",
                CardType = 1,
                City = "Tallinn",
                Country = "Estonia",
                Email = "demouser@cetiya.com",
                Expiration = "12/20",
                Id = Guid.NewGuid().ToString(),
                LastName = "DemoLastName",
                Name = "DemoUser",
                PhoneNumber = "1234567890",
                UserName = "demo",
                ZipCode = "12915",
                State = "-",
                Street = "LAKI 24",
                SecurityNumber = "535",
                NormalizedEmail = "DEMOUSER@CETIYA.COM",
                NormalizedUserName = "DEMO",
                SecurityStamp = Guid.NewGuid().ToString("D"),
            };

            user.PasswordHash = _passwordHasher.HashPassword(user, "1234");

            return new List<ApplicationUser>()
            {
                user
            };
        }
    }
}
