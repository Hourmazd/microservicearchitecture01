using System;
using System.Threading.Tasks;
using App.Metrics;
using App.Metrics.AspNetCore;
using App.Metrics.Filtering;
using App.Metrics.Formatters.Json;
using IdentityServer.Application.DataSeeders;
using IdentityServer.Persistence;
using IdentityServer4.EntityFramework.DbContexts;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Events;
using Serilog.Sinks.SystemConsole.Themes;

namespace IdentityServer.API
{
    public class Program
    {
        public static readonly string Namespace = typeof(Program).Namespace;
        public static readonly string AppName = Namespace.Substring(Namespace.LastIndexOf('.', Namespace.LastIndexOf('.') - 1) + 1);

        public static async Task<int> Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
                .MinimumLevel.Override("System", LogEventLevel.Warning)
                .MinimumLevel.Override("Microsoft.AspNetCore.Authentication", LogEventLevel.Information)
                .Enrich.FromLogContext()
                // uncomment to write to Azure diagnostics stream
                //.WriteTo.File(
                //    @"D:\home\LogFiles\Application\identityserver.txt",
                //    fileSizeLimitBytes: 1_000_000,
                //    rollOnFileSizeLimit: true,
                //    shared: true,
                //    flushToDiskInterval: TimeSpan.FromSeconds(1))
                .WriteTo.Console(outputTemplate: "[{Timestamp:HH:mm:ss} {Level}] {SourceContext}{NewLine}{Message:lj}{NewLine}{Exception}{NewLine}", theme: AnsiConsoleTheme.Literate)
                .CreateLogger();

            try
            {
                Log.Information("Configuring web host ({ApplicationContext})...", AppName);
                var host = CreateHostBuilder(args).Build();

                using (var scope = host.Services.CreateScope())
                {
                    var services = scope.ServiceProvider;

                    Log.Information("Applying migrations ({Context})...", typeof(ApplicationDbContext).Name);
                    var applicationdbContext = services.GetRequiredService<ApplicationDbContext>();
                    applicationdbContext.Database.Migrate();

                    Log.Information("Applying migrations ({Context})...", typeof(PersistedGrantDbContext).Name);
                    var persistedGrantdbContext = services.GetRequiredService<PersistedGrantDbContext>();
                    persistedGrantdbContext.Database.Migrate();

                    Log.Information("Applying migrations ({Context})...", typeof(ConfigurationDbContext).Name);
                    var configurationdbContext = services.GetRequiredService<ConfigurationDbContext>();
                    configurationdbContext.Database.Migrate();

                    var logger1 = services.GetService<ILogger<ApplicationDataSeeder>>();
                    var appSeeder = new ApplicationDataSeeder();
                    await appSeeder.SeedAsync(applicationdbContext, logger1);

                    var logger2 = services.GetService<ILogger<ConfigurationDataSeeder>>();
                    var configuration = services.GetService<IConfiguration>();
                    var configSeeder = new ConfigurationDataSeeder();
                    await configSeeder.SeedAsync(configurationdbContext, configuration, logger2);
                }

                Log.Information("Starting web host ({ApplicationContext})...", AppName);
                host.Run();

                return 0;
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Program terminated unexpectedly ({ApplicationContext})!", AppName);
                return 1;
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration((context, config) =>
                {
                    var env = context.HostingEnvironment;

                    config.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                        .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true, reloadOnChange: true)
                        .AddJsonFile("appsettings.Local.json", optional: true, reloadOnChange: true);

                    config.AddEnvironmentVariables();
                })
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    var filter = new MetricsFilter().WhereType(MetricType.Timer);

                    webBuilder
                        .ConfigureMetricsWithDefaults((context, builder) =>
                        {
                            builder.Report.ToTextFile(options =>
                            {
                                options.MetricsOutputFormatter = new MetricsJsonOutputFormatter();
                                options.AppendMetricsToTextFile = true;
                                options.Filter = filter;
                                options.FlushInterval = TimeSpan.FromSeconds(20);
                                options.OutputPathAndFileName = @"e:\metrics.identity.txt";
                            });
                        })
                        .UseSerilog((host, config) =>
                        {
                            config.WriteTo.Console();
                            config.WriteTo.File(@"e:/log.identity.txt", LogEventLevel.Information);
                        })
                        .UseMetrics()
                        .UseStartup<Startup>();
                });
    }
}
