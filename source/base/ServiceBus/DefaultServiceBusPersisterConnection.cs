﻿using System;
using Microsoft.Azure.ServiceBus;
using Microsoft.Extensions.Logging;

namespace ServiceBus
{
    public class DefaultServiceBusPersisterConnection : IServiceBusPersisterConnection
    {
        public ServiceBusConnectionStringBuilder ServiceBusConnectionStringBuilder { get; }

        private readonly ILogger<DefaultServiceBusPersisterConnection> _Logger;
        private ITopicClient _TopicClient;
        private bool _Disposed;
        
        public DefaultServiceBusPersisterConnection(
            ServiceBusConnectionStringBuilder serviceBusConnectionStringBuilder,
            ILogger<DefaultServiceBusPersisterConnection> logger)
        {
            _Logger = logger ?? throw new ArgumentNullException(nameof(logger));

            ServiceBusConnectionStringBuilder = 
                serviceBusConnectionStringBuilder ??
                throw new ArgumentNullException(nameof(serviceBusConnectionStringBuilder));

            //_TopicClient = new TopicClient(ServiceBusConnectionStringBuilder, RetryPolicy.Default);
        }

        public ITopicClient CreateModel()
        {
            if (_TopicClient == null || _TopicClient.IsClosedOrClosing)
                _TopicClient = new TopicClient(ServiceBusConnectionStringBuilder, RetryPolicy.Default);

            return _TopicClient;
        }

        public void Dispose()
        {
            if (_Disposed) return;

            _Disposed = true;
        }
    }
}
