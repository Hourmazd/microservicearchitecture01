﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.ServiceBus;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ServiceBus
{
    public class EventBus : IEventBus
    {
        private readonly IServiceProvider _ServiceProvider;
        private readonly IServiceBusPersisterConnection _ServiceBusConnection;
        private readonly ILogger<EventBus> _Logger;
        private readonly IEventBusSubscriptionsManager _SubscriptionManager;
        private readonly SubscriptionClient _SubscriptionClient;
        //private readonly ILifetimeScope _autofac;
        //private readonly string AUTOFAC_SCOPE_NAME = "eshop_event_bus";
        private const string INTEGRATION_EVENT_SUFFIX = "IntegrationEvent";

        public EventBus(
            IServiceProvider serviceProvider,
            IServiceBusPersisterConnection serviceBusPersisterConnection,
            ILogger<EventBus> logger,
            IEventBusSubscriptionsManager subsManager)
        {
            _ServiceProvider = serviceProvider;
            _ServiceBusConnection = serviceBusPersisterConnection;
            _Logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _SubscriptionManager = subsManager ?? throw new ArgumentNullException(nameof(subsManager));

            _SubscriptionClient = new SubscriptionClient(serviceBusPersisterConnection.ServiceBusConnectionStringBuilder, "order");

            RemoveDefaultRule();
            RegisterSubscriptionClientMessageHandler();
        }

        public async Task Publish(IntegrationEvent @event)
        {
            var eventName = @event.GetType().Name.Replace(INTEGRATION_EVENT_SUFFIX, "");
            var jsonMessage = JsonConvert.SerializeObject(@event);
            var body = Encoding.UTF8.GetBytes(jsonMessage);

            var message = new Message
            {
                MessageId = Guid.NewGuid().ToString(),
                Body = body,
                Label = eventName,
            };

            var topicClient = _ServiceBusConnection.CreateModel();

            await topicClient.SendAsync(message);
        }

        public async Task Subscribe<T, TH>()
            where T : IntegrationEvent
            where TH : IIntegrationEventHandler<T>
        {
            var eventName = typeof(T).Name.Replace(INTEGRATION_EVENT_SUFFIX, "");

            var containsKey = _SubscriptionManager.HasSubscriptionsForEvent<T>();
            if (!containsKey)
            {
                try
                {
                    await _SubscriptionClient.AddRuleAsync(new RuleDescription
                    {
                        Filter = new CorrelationFilter { Label = eventName },
                        Name = eventName
                    });
                }
                catch (ServiceBusException)
                {
                    _Logger.LogWarning("The messaging entity {eventName} already exists.", eventName);
                }
            }

            _Logger.LogInformation("Subscribing to event {EventName} with {EventHandler}", eventName, typeof(TH).Name);

            _SubscriptionManager.AddSubscription<T, TH>();
        }

        private void RegisterSubscriptionClientMessageHandler()
        {
            _SubscriptionClient.RegisterMessageHandler(
                async (message, token) =>
                {
                    var eventName = $"{message.Label}{INTEGRATION_EVENT_SUFFIX}";
                    var messageData = Encoding.UTF8.GetString(message.Body);

                    // Complete the message so that it is not received again.
                    if (await ProcessEvent(eventName, messageData))
                    {
                        await _SubscriptionClient.CompleteAsync(message.SystemProperties.LockToken);
                    }
                },
                new MessageHandlerOptions(ExceptionReceivedHandler) { MaxConcurrentCalls = 10, AutoComplete = false });
        }

        private Task ExceptionReceivedHandler(ExceptionReceivedEventArgs exceptionReceivedEventArgs)
        {
            var ex = exceptionReceivedEventArgs.Exception;
            var context = exceptionReceivedEventArgs.ExceptionReceivedContext;

            _Logger.LogError(ex, "ERROR handling message: {ExceptionMessage} - Context: {@ExceptionContext}", ex.Message, context);

            return Task.CompletedTask;
        }

        private async Task<bool> ProcessEvent(string eventName, string message)
        {
            var processed = false;

            if (_SubscriptionManager.HasSubscriptionsForEvent(eventName))
            {
                var subscriptions = _SubscriptionManager.GetHandlersForEvent(eventName);
                foreach (var subscription in subscriptions)
                {
                    try
                    {
                        if (subscription.IsDynamic)
                        {
                            var handler = _ServiceProvider.GetService(subscription.HandlerType) as IDynamicIntegrationEventHandler;

                            if (handler == null)
                                continue;

                            dynamic eventData = JObject.Parse(message);
                            await handler.Handle(eventData);
                        }
                        else
                        {
                            var handler = _ServiceProvider.GetService(subscription.HandlerType);

                            if (handler == null)
                                continue;

                            var eventType = _SubscriptionManager.GetEventTypeByName(eventName);
                            var integrationEvent = JsonConvert.DeserializeObject(message, eventType);
                            var concreteType = typeof(IIntegrationEventHandler<>).MakeGenericType(eventType);
                            var handleMethod = concreteType.GetMethod("Handle");

                            if (handleMethod != null)
                                await (Task)handleMethod.Invoke(handler, new[] { integrationEvent });
                        }
                    }
                    catch (Exception e)
                    {
                        _Logger.LogError($"Unable to process event {eventName}.", e);
                    }
                }
                processed = true;
            }

            return processed;
        }

        private void RemoveDefaultRule()
        {
            try
            {
                _SubscriptionClient
                 .RemoveRuleAsync(RuleDescription.DefaultRuleName)
                 .GetAwaiter()
                 .GetResult();
            }
            catch (MessagingEntityNotFoundException)
            {
                _Logger.LogWarning("The messaging entity {DefaultRuleName} Could not be found.", RuleDescription.DefaultRuleName);
            }
        }

        public void Dispose()
        {
            _SubscriptionManager.Dispose();
            _ServiceBusConnection?.Dispose();
        }
    }
}
