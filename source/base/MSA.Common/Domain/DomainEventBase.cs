﻿using MediatR;

namespace MSA.Common.Domain
{
    public abstract class DomainEventBase : INotification
    {
        protected DomainEventBase(int entityId)
        {
            EntityId = entityId;
        }

        public int EntityId { get; }
    }
}
