﻿namespace MSA.Common.EventBus
{
    public class AzureServiceBusSetting
    {
        public string Endpoint { get; set; }
        public string EntityPath { get; set; }
        public string SharedAccessKeyName { get; set; }
        public string SharedAccessKey { get; set; }
        public string SubscriptionClientName { get; set; }        
    }
}
