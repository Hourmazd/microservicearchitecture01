﻿using System;
using System.Threading.Tasks;

namespace MSA.Common.EventBus
{
    public interface IEventBus : IDisposable
    {
        Task Subscribe<T>()
            where T : IIntegrationEvent;

        Task Publish(IIntegrationEvent @event);
    }
}
