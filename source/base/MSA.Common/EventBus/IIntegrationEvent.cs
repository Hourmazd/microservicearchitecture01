﻿using System;

namespace MSA.Common.EventBus
{
    public interface IIntegrationEvent
    {
        Guid Id { get; }

        DateTime CreationDate { get; }
    }
}
