﻿using Microsoft.Azure.ServiceBus;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Threading;
using Microsoft.Azure.ServiceBus.Management;

namespace MSA.Common.EventBus
{
    public abstract class AzureServiceBus : IEventBus
    {
        private ServiceBusConnectionStringBuilder _ServiceBusConnectionStringBuilder;
        private SubscriptionClient _SubscriptionClient;
        protected readonly ILogger<AzureServiceBus> _Logger;
        protected readonly IDictionary<string, Type> _SubscriptionTypes;

        private ITopicClient _TopicClient;

        private ITopicClient TopicClient
        {
            get
            {
                if (_TopicClient == null || _TopicClient.IsClosedOrClosing)
                    _TopicClient = new TopicClient(_ServiceBusConnectionStringBuilder, Microsoft.Azure.ServiceBus.RetryPolicy.Default);

                return _TopicClient;
            }
        }

        protected AzureServiceBus(IConfiguration configuration, ILogger<AzureServiceBus> logger)
        {
            _Logger = logger;

            ConfigureServiceBus(configuration).GetAwaiter().GetResult();

            _SubscriptionTypes = new Dictionary<string, Type>();

            RegisterSubscriptionClientMessageHandler();
        }

        protected virtual async Task ConfigureServiceBus(IConfiguration configuration)
        {
            var setting = new AzureServiceBusSetting();
            configuration.GetSection("EventBus").Bind(setting);

            _ServiceBusConnectionStringBuilder =
                new ServiceBusConnectionStringBuilder(setting.Endpoint, setting.EntityPath, setting.SharedAccessKeyName, setting.SharedAccessKey);

            var managementClient = new ManagementClient(_ServiceBusConnectionStringBuilder);

            if (!await managementClient.TopicExistsAsync(setting.EntityPath))
            {
                var topic = new TopicDescription(setting.EntityPath)
                {
                    RequiresDuplicateDetection = false
                };

                await managementClient.CreateTopicAsync(topic);
            }

            if (!await managementClient.SubscriptionExistsAsync(setting.EntityPath, setting.SubscriptionClientName))
            {
                var subscription = new SubscriptionDescription(setting.EntityPath, setting.SubscriptionClientName)
                {
                    EnableBatchedOperations = true,
                    AutoDeleteOnIdle = TimeSpan.FromDays(100),
                    EnableDeadLetteringOnMessageExpiration = true,
                    DefaultMessageTimeToLive = TimeSpan.FromDays(100),
                    MaxDeliveryCount = 100,
                    LockDuration = TimeSpan.FromMinutes(5)
                };

                await managementClient.CreateSubscriptionAsync(subscription);
            }

            _SubscriptionClient = new SubscriptionClient(_ServiceBusConnectionStringBuilder, setting.SubscriptionClientName);

            var rules = await _SubscriptionClient.GetRulesAsync();
            var defaultRule = rules.FirstOrDefault(e => e.Name == "$Default");

            if (defaultRule != null)
                await _SubscriptionClient.RemoveRuleAsync(defaultRule.Name);
        }

        public async Task Subscribe<T>()
            where T : IIntegrationEvent
        {
            var eventName = typeof(T).Name;
            var ruleName = eventName.Replace("IntegrationEvent", string.Empty);
            var rules = await _SubscriptionClient.GetRulesAsync();

            if (rules.All(e => e.Name != ruleName))
            {
                try
                {
                    await _SubscriptionClient.AddRuleAsync(new RuleDescription
                    {
                        Filter = new CorrelationFilter { Label = eventName },
                        Name = ruleName
                    });

                    _Logger.LogInformation("Subscribing to event {EventName}", eventName);
                }
                catch (ServiceBusException)
                {
                    _Logger.LogWarning("The messaging entity {eventName} already exists.", eventName);
                }
            }

            if (!_SubscriptionTypes.ContainsKey(eventName))
                _SubscriptionTypes.Add(eventName, typeof(T));
        }

        public async Task Publish(IIntegrationEvent @event)
        {
            var eventName = @event.GetType().Name;
            var jsonMessage = JsonConvert.SerializeObject(@event);
            var body = Encoding.UTF8.GetBytes(jsonMessage);

            var message = new Message
            {
                MessageId = Guid.NewGuid().ToString(),
                Body = body,
                Label = eventName
            };

            await TopicClient.SendAsync(message);
        }

        private void RegisterSubscriptionClientMessageHandler()
        {
            _SubscriptionClient.RegisterMessageHandler
                (Handeller, new MessageHandlerOptions(ExceptionReceivedHandler) { AutoComplete = false, MaxConcurrentCalls = 10 });
        }

        protected async Task Handeller(Message message, CancellationToken token = default)
        {
            if (await ProcessEvent(message))
            {
                await _SubscriptionClient.CompleteAsync(message.SystemProperties.LockToken);
            }
        }

        protected abstract Task<bool> ProcessEvent(Message message);

        private Task ExceptionReceivedHandler(ExceptionReceivedEventArgs ex)
        {
            _Logger.LogError(ex.Exception, $"Unable to handle integration event in {ex.ExceptionReceivedContext.EntityPath}.");

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            if (_TopicClient != null)
                _TopicClient.CloseAsync().GetAwaiter().GetResult();
        }
    }
}
