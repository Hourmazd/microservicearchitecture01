﻿using System;
using System.Threading.Tasks;

namespace MSA.Common.EventBus
{
    public class RabbitMQServiceBus : IEventBus, IDisposable
    {
        //TODO: Implement for each service same as (AzureServiceBus)

        public Task Subscribe<T>() where T : IIntegrationEvent
        {
           return Task.CompletedTask;
        }

        public Task Publish(IIntegrationEvent @event)
        {
           return Task.CompletedTask;
        }

        public void Dispose()
        {
        }
    }
}
