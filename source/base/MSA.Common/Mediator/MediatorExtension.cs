﻿using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using MSA.Common.Domain;

namespace MSA.Common.Mediator
{
    public static class MediatorExtension
    {
        public static async Task DispatchDomainEventsAsync(this IMediator mediator, DbContext context)
        {
            var domainEntities = context
                .ChangeTracker
                .Entries<EntityBase>()
                .Where(e => e.Entity.DomainEvents != null && e.Entity.DomainEvents.Any()).ToList();

            var domainEvents = domainEntities.SelectMany(e => e.Entity.DomainEvents).ToList();

            domainEntities.ToList().ForEach(e => e.Entity.ClearDomainEvents());

            foreach (var @event in domainEvents)
                await mediator.Publish(@event);
        }
    }
}
