﻿using Microsoft.EntityFrameworkCore.Query;
using MSA.Common.Domain;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace MSA.Common.Persistence
{
    public interface IDataFetchSpecification<TEntity>
        where TEntity : EntityBase
    {
        Expression<Func<TEntity, bool>> Criteria { get; set; }

        Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> SortExpression { get; set; }

        Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> IncludeExpression { get; set; }

        bool IsPagingEnabled { get; set; }

        int Page { get; set; }

        int PageCount { get; set; }

        IQueryable<TEntity> Apply(IQueryable<TEntity> query);
    }
}
