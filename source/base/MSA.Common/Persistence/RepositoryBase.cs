﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using MSA.Common.Domain;

namespace MSA.Common.Persistence
{
    public abstract class RepositoryBase<TContext, TEntity> : IRepositoryBase<TEntity>
        where TContext : DbContextBase, IDbContextBase
        where TEntity : EntityBase
    {
        protected RepositoryBase(TContext context, IMediator mediator)
        {
            Context = context ?? throw new ArgumentNullException("Context could not be null.");
            Mediator = mediator ?? throw new ArgumentNullException("Mediator could not be null.");
        }

        protected TContext Context { get; }
        protected IMediator Mediator { get; }
        protected DbSet<TEntity> DbSet => Context.Set<TEntity>();

        public virtual IQueryable<TEntity> QueryableList => Context.Set<TEntity>().AsQueryable<TEntity>();

        public async Task<TEntity> Add(TEntity entity, CancellationToken cancellationToken = default)
        {
            DbSet.Add(entity);

            await SaveEntitiesAsync(cancellationToken);

            return entity;
        }

        public async Task<bool> Remove(TEntity entity, CancellationToken cancellationToken = default)
        {
            if (entity == null)
                throw new ArgumentException($"Unable to find {typeof(TEntity)} entity with Id {entity.Id}");

            Context.Entry(entity).State = EntityState.Deleted;

            var result = await SaveEntitiesAsync(cancellationToken);

            return result > 0;
        }

        public async Task<bool> Update(TEntity entity, CancellationToken cancellationToken = default)
        {
            Context.Entry(entity).State = EntityState.Modified;

            var result = await SaveEntitiesAsync(cancellationToken);

            return result > 0;
        }

        public virtual async Task<TEntity> GetEntityById(int entityId, CancellationToken cancellationToken = default)
        {
            var result = await DbSet.FindAsync(new object[] { entityId }, cancellationToken);

            if (result == null)
            {
                result = DbSet.Local.FirstOrDefault(e => e.Id == entityId);
            }
            else
            {
                await LoadRelatedEntitiesOnSingleEntity(result);
            }

            return result;
        }

        public async Task<IEnumerable<TEntity>> GetList(IDataFetchSpecification<TEntity> specification = null, CancellationToken cancellationToken = default)
        {
            var query = QueryableList;

            if (specification != null)
                query = specification.Apply(query);
            
            return await query.ToListAsync(cancellationToken);
        }

        protected virtual async Task<int> SaveEntitiesAsync(CancellationToken cancellationToken = default)
        {
            var domainEntities = Context
                .ChangeTracker
                .Entries<TEntity>()
                .Where(e => e.Entity.DomainEvents != null && e.Entity.DomainEvents.Any());

            var domainEvents = domainEntities.SelectMany(e => e.Entity.DomainEvents).ToList();

            var result = await Context.SaveChangesAsync(cancellationToken);

            foreach (var @event in domainEvents)
                await Mediator.Publish(@event, cancellationToken);

            domainEntities.ToList().ForEach(e => e.Entity.ClearDomainEvents());

            return result;
        }

        protected virtual Task LoadRelatedEntitiesOnSingleEntity(TEntity entity)
        {
            return Task.CompletedTask;
        }
    }
}
