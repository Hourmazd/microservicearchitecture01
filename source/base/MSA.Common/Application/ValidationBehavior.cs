﻿using MediatR;
using Microsoft.Extensions.Logging;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using System.Linq;
using MSA.Common.Basic;

namespace MSA.Common.Application
{
    public class ValidationBehavior<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse>
        where TRequest : IRequest<TResponse>
    {
        private readonly ILogger<ValidationBehavior<TRequest, TResponse>> _Logger;
        private readonly IValidator<TRequest>[] _Validators;

        public ValidationBehavior(IValidator<TRequest>[] validators, ILogger<ValidationBehavior<TRequest, TResponse>> logger)
        {
            _Validators = validators;
            _Logger = logger;
        }

        public async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
        {
            var typeName = request.GetGenericTypeName();

            var failures = _Validators
                .Select(v => v.Validate(request))
                .SelectMany(result => result.Errors)
                .Where(error => error != null)
                .ToList();

            if (failures.Any())
            {
                _Logger.LogWarning("\t>> Validation errors - {CommandType} - Command: {@Command} - Errors: {@ValidationErrors}", typeName, request, failures);

                throw new ValidationException("Validation error.", failures);
            }

            return await next();
        }
    }
}
