﻿using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using MSA.Common.Basic;

namespace MSA.Common.Application
{
    public class LoggingBehavior<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse>
        where TRequest : IRequest<TResponse>
    {
        private readonly ILogger<LoggingBehavior<TRequest, TResponse>> _Logger;
        private readonly Stopwatch _Timer;

        public LoggingBehavior(ILogger<LoggingBehavior<TRequest, TResponse>> logger)
        {
            _Logger = logger;
            _Timer = new Stopwatch();
        }

        public async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
        {
            _Timer.Start();

            var response = await next();

            _Timer.Stop();

            var requestType = request != null ? request.GetGenericTypeName() : "NULL";
            var responseType = response != null ? response.GetGenericTypeName() : "NULL";

            _Logger.LogInformation("\t>> Command {CommandName} handled - response: {@Response} ({ElapsedMilliseconds} ms)", requestType, responseType, _Timer.ElapsedMilliseconds);

            return response;
        }
    }
}
