﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using MSA.Common.Application;
using MSA.Common.Domain;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;

namespace MSA.Common.API
{
    [Route("api/[controller]")]
    [ApiController]
    public abstract class BaseController<TEntity, TDTO> : ControllerBase
        where TEntity : EntityBase
        where TDTO : DTOBase
    {
        private IMediator _Mediator;

        protected IMediator Mediator => _Mediator ??= HttpContext.RequestServices.GetService<IMediator>();

        protected abstract IRequest<IEnumerable<TDTO>> FetchListCommand();

        protected abstract IRequest<TDTO> FetchSingleCommand(int entityId);

        protected abstract IRequest<bool> DeleteCommand(int entityId);

        [Route("[action]")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TDTO>>> GetAll(CancellationToken cancellationToken)
        {
            var accessToken = await HttpContext.GetTokenAsync("access_token");
            var idToken = await HttpContext.GetTokenAsync("id_token");
            var refreshToken = await HttpContext.GetTokenAsync("refresh_token");

            if (!string.IsNullOrEmpty(accessToken))
            {
                var _accessToken = new JwtSecurityTokenHandler().ReadJwtToken(accessToken);
            }

            if (!string.IsNullOrEmpty(idToken))
            {
                var _idToken = new JwtSecurityTokenHandler().ReadJwtToken(idToken);
            }

            return Ok(await Mediator.Send(FetchListCommand(), cancellationToken));
        }

        [Route("[action]/{entityId:int}")]
        [HttpGet]
        public async Task<ActionResult<TDTO>> Get(int entityId, CancellationToken cancellationToken)
        {
            var accessToken = await HttpContext.GetTokenAsync("access_token");
            var idToken = await HttpContext.GetTokenAsync("id_token");
            var refreshToken = await HttpContext.GetTokenAsync("refresh_token");

            if (!string.IsNullOrEmpty(accessToken))
            {
                var _accessToken = new JwtSecurityTokenHandler().ReadJwtToken(accessToken);
            }

            if (!string.IsNullOrEmpty(idToken))
            {
                var _idToken = new JwtSecurityTokenHandler().ReadJwtToken(idToken);
            }

            return Ok(await Mediator.Send(FetchSingleCommand(entityId), cancellationToken));
        }

        [Route("[action]")]
        [HttpPost]
        public async Task<ActionResult<TDTO>> Upsert(TDTO dto, CancellationToken cancellationToken)
        {
            var result = await Mediator.Send(dto, cancellationToken);

            return await Get(((TEntity)result).Id, cancellationToken);
        }

        [Route("[action]/{entityId:int}")]
        [HttpDelete]
        public async Task<ActionResult> Delete(int entityId, CancellationToken cancellationToken)
        {
            return Ok(await Mediator.Send(DeleteCommand(entityId), cancellationToken));
        }
    }
}