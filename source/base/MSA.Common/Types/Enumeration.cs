﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace MSA.Common.Types
{
    public abstract class Enumeration : IComparable
    {
        #region Constructors

        protected Enumeration()
        {
        }

        protected Enumeration(int id, string name)
        {
            Id = id;
            Name = name;
        }

        #endregion

        #region Public Properties

        public string Name { get; private set; }

        public int Id { get; private set; }

        #endregion

        #region Public Methods

        public static IEnumerable<T> GetItems<T>() where T : Enumeration
        {
            var fields = typeof(T).GetFields(BindingFlags.Public | BindingFlags.Static | BindingFlags.DeclaredOnly);
            return fields.Select(e => e.GetValue(null)).Cast<T>();
        }

        public static T FromValue<T>(int value) where T : Enumeration
        {
            var matchingItem = Parse<T, int>(value, "value", item => item.Id == value);
            return matchingItem;
        }

        public static T FromName<T>(string displayName) where T : Enumeration
        {
            var matchingItem = Parse<T, string>(displayName, "display name", item => item.Name == displayName);
            return matchingItem;
        }

        private static T Parse<T, K>(K value, string description, Func<T, bool> predicate) where T : Enumeration
        {
            var matchingItem = GetItems<T>().FirstOrDefault(predicate);

            if (matchingItem == null)
                throw new InvalidOperationException($"'{value}' is not a valid {description} in {typeof(T)}");

            return matchingItem;
        }

        #endregion

        #region IComparable Members

        public int CompareTo(object obj) => Id.CompareTo(((Enumeration)obj).Id);

        #endregion

        #region Override Members

        public override string ToString() => Name;

        public override bool Equals(object obj)
        {
            var other = obj as Enumeration;

            if (other == null)
                return false;

            var typeMatches = GetType() == obj.GetType();
            var valueMatches = Id.Equals(other.Id);

            return typeMatches && valueMatches;
        }

        public override int GetHashCode() => Id.GetHashCode();

        public static bool operator ==(Enumeration left, Enumeration right)
        {
            if (Equals(left, null))
                return Equals(right, null);
            else
                return left.Equals(right);
        }

        public static bool operator !=(Enumeration left, Enumeration right)
        {
            return !(left == right);
        }

        #endregion
    }
}
