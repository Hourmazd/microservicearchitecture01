﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using IdentityModel.Client;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;

namespace ApiGateway.API.IdentityMiddleware
{
    public static class IdentityServerMiddleware
    {
        public static IApplicationBuilder UseIdentityServerApiGateway(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<RequestClientCredentialsTokenMiddleware>();
        }
    }

    public class RequestClientCredentialsTokenMiddleware
    {
        private readonly RequestDelegate _Next;

        public RequestClientCredentialsTokenMiddleware(RequestDelegate next)
        {
            _Next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            var authorizationHeader = context.Request.Headers["Authorization"];

            if (!string.IsNullOrEmpty(authorizationHeader))
            {
                context.Request.Headers.Add("Authorization", "");
            }

            var token = await GetClientCredentialsToken();

            if (token != null)
            {
                var bearer = new AuthenticationHeaderValue("Bearer", token);
                context.Request.Headers["Authorization"] = bearer.ToString();
            }

            // Call the next delegate/middleware in the pipeline
            await _Next(context);
        }

        private async Task<string> GetClientCredentialsToken()
        {
            var client = new HttpClient();
            var disco =
                await client.GetDiscoveryDocumentAsync("https://localhost:5001");

            if (disco.IsError)
                throw new Exception(disco.Error);

            var tokenResponse = await client.RequestClientCredentialsTokenAsync(
                new ClientCredentialsTokenRequest
                {
                    Address = disco.TokenEndpoint,
                    
                    ClientId = "ApiGateway",
                    ClientSecret = "secret",
                    //Scope = "ProductAPI",
                });

            if (tokenResponse.IsError)
                throw new Exception(tokenResponse.Error);

            return tokenResponse.AccessToken;
        }
    }
}
