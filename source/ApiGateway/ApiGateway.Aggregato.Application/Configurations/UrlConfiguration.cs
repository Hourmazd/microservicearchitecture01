﻿namespace ApiGateway.Aggregator.Application.Configurations
{
    public class UrlConfiguration
    {
        public static string IdentityServer { get; set; }
        public static string OrderApi { get; set; }
        public static string ProductApi { get; set; }
        public static string WarehouseApi { get; set; }

        public class OrderUrls
        {
            public static string GetOrder (int id) => $"{OrderApi}/api/order/get/{id}";
        }

        public class ProductUrls
        {
            public static string GetProduct(int id) => $"{ProductApi}/api/product/get/{id}";
        }

        public class WarehouseUrls
        {
            public static string GetWarehouses() => $"{WarehouseApi}/api/warehouse/getall";
            public static string GetWarehouse(int id) => $"{WarehouseApi}/api/warehouse/get/{id}";
        }
    }
}
