﻿using ApiGateway.Aggregator.Application.IServices;
using ApiGateway.Aggregator.Application.Middlewares;
using ApiGateway.Aggregator.Application.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ApiGateway.Aggregator.Application
{
    public static class DependencyInjector
    {
        public static IServiceCollection AddApplication(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddHttpContextAccessor();
            services.AddTransient<HttpClientAuthorizationDelegatingHandler>();

            services
                .AddHttpClient<IProductService, ProductService>();
                //.AddHttpMessageHandler<HttpClientAuthorizationDelegatingHandler>();

            //services.AddTransient<IProductService, ProductService>();
            //services.AddTransient<IOrderService, OrderService>();
            //services.AddTransient<IWarehouseService, WarehouseService>();

            return services;
        }
    }
}