﻿using MSA.Common.Application;

namespace ApiGateway.Aggregator.Application.Models
{
    public class OrderItemDTO : ViewModelBase
    {
        public int ProductId { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
    }
}
