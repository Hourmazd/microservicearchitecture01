﻿namespace ApiGateway.Aggregator.Application.Models
{
    public class WarehouseItemDTO : ViewModelBase
    {
        public int ProductId { get; set; }

        public int StoreQuantity { get; set; }

        public int BookedQuantity { get; set; }

        public int AvailableQuantity { get; set; }
    }
}
