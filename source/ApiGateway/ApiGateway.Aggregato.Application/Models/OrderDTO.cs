﻿using System;
using System.Collections.Generic;
using MSA.Common.Application;

namespace ApiGateway.Aggregator.Application.Models
{
    public class OrderDTO : ViewModelBase
    {
        public DateTime OrderDate { get; set; }

        public int StatusId { get; set; }

        public string StatusName { get; set; }

        public IEnumerable<OrderItemDTO> OrderItems { get; set; }
    }
}
