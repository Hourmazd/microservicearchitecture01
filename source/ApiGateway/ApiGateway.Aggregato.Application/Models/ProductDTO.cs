﻿namespace ApiGateway.Aggregator.Application.Models
{
    public class ProductDTO : ViewModelBase
    {
        public string Code { get; set; }

        public string Title { get; set; }

        public decimal Price { get; set; }

        public int CategoryId { get; set; }

        public string CategoryName { get; set; }

        public int StatusId { get; set; }

        public string StatusName { get; set; }

        public int AvailableQuantity { get; set; }
    }
}
