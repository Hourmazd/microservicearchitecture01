﻿using System.Collections.Generic;

namespace ApiGateway.Aggregator.Application.Models
{
    public class WarehouseDTO : ViewModelBase
    {
        public string Code { get; set; }

        public IEnumerable<WarehouseItemDTO> Items { get; set; }
    }
}
