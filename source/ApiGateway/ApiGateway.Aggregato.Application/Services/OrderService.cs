﻿using System.Net.Http;
using ApiGateway.Aggregator.Application.Configurations;
using ApiGateway.Aggregator.Application.IServices;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace ApiGateway.Aggregator.Application.Services
{
    public class OrderService : IOrderService
    {
        private readonly IHttpClientFactory _HttpClientFactory;
        private readonly ILogger<OrderService> _Logger;
        private readonly UrlConfiguration _Urls;

        public OrderService(
            IHttpClientFactory httpClientFactory, 
            ILogger<OrderService> logger, 
            IOptions<UrlConfiguration> urls)
        {
            _HttpClientFactory = httpClientFactory;
            _Logger = logger;
            _Urls = urls.Value;
        }
    }
}
