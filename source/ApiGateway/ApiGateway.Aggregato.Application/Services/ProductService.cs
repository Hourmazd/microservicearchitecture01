﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ApiGateway.Aggregator.Application.IServices;
using ApiGateway.Aggregator.Application.Models;
using ApiGateway.Aggregator.Application.Configurations;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace ApiGateway.Aggregator.Application.Services
{
    public class ProductService : IProductService
    {
        private readonly HttpClient _HttpClient;

        public ProductService(HttpClient httpClient)
        {
            _HttpClient = httpClient;
        }

        public async Task<ProductDTO> GetProduct(int id, CancellationToken cancellationToken)
        {
            var result = default(ProductDTO);
            var response = await _HttpClient.GetAsync(UrlConfiguration.ProductUrls.GetProduct(id), cancellationToken);

            if (response.IsSuccessStatusCode)
            {
                var str = await response.Content.ReadAsStringAsync();
                result = JsonConvert.DeserializeObject<ProductDTO>(str);

                if (result != default)
                {
                    //Getting stored quantity from Warehouse
                    var wresponse =
                        await _HttpClient.GetAsync(UrlConfiguration.WarehouseUrls.GetWarehouses(), cancellationToken);

                    if (wresponse.IsSuccessStatusCode)
                    {
                        var wStr = await wresponse.Content.ReadAsStringAsync();
                        var wresult = !string.IsNullOrEmpty(wStr)
                            ? JsonConvert.DeserializeObject<IEnumerable<WarehouseDTO>>(wStr)
                            : new List<WarehouseDTO>();

                        result.AvailableQuantity = wresult.SelectMany(e => e.Items).Sum(e => e.AvailableQuantity);
                    }
                    else
                    {
                        //_Logger.LogError(wresponse.ReasonPhrase);
                    }
                }
            }
            else
            {
               // _Logger.LogError(response.ReasonPhrase);
            }

            return result;
        }
    }
}
