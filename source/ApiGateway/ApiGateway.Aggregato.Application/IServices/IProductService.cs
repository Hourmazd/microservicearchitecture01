﻿using System.Threading;
using System.Threading.Tasks;
using ApiGateway.Aggregator.Application.Models;

namespace ApiGateway.Aggregator.Application.IServices
{
    public interface IProductService
    {
        Task<ProductDTO> GetProduct(int id, CancellationToken cancellationToken);
    }
}
