﻿using System.Threading;
using System.Threading.Tasks;
using ApiGateway.Aggregator.Application.IServices;
using ApiGateway.Aggregator.Application.Models;
using Microsoft.AspNetCore.Mvc;

namespace ApiGateway.Aggregator.API.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductService ProductService;

        public ProductController(IProductService productService)
        {
            ProductService = productService;
        }

        [HttpGet]
        [Route("{id:int}")]
        public async Task<ActionResult<ProductDTO>> Get(int id, CancellationToken cancellationToken)
        {
            return Ok(await ProductService.GetProduct(id, cancellationToken));
        }
    }
}