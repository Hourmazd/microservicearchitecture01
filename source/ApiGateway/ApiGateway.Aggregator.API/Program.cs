using System;
using App.Metrics;
using App.Metrics.AspNetCore;
using App.Metrics.Filtering;
using App.Metrics.Formatters.Json;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Events;

namespace ApiGateway.Aggregator.API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration((context, config) =>
                {
                    var env = context.HostingEnvironment;

                    config.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                        .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true, reloadOnChange: true)
                        .AddJsonFile("appsettings.Local.json", optional: true, reloadOnChange: true);

                    config.AddEnvironmentVariables();
                })
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    var filter = new MetricsFilter().WhereType(MetricType.Timer);

                    webBuilder
                        .ConfigureMetricsWithDefaults((context, builder) =>
                        {
                            builder.Report.ToTextFile(options =>
                            {
                                options.MetricsOutputFormatter = new MetricsJsonOutputFormatter();
                                options.AppendMetricsToTextFile = true;
                                options.Filter = filter;
                                options.FlushInterval = TimeSpan.FromSeconds(20);
                                options.OutputPathAndFileName = @"e:\metrics.aggregator.txt";
                            });
                        })
                        .UseSerilog((host, config) =>
                        {
                            config.WriteTo.Console();
                            config.WriteTo.File(@"e:/log.aggregator.txt", LogEventLevel.Information);
                        })
                        .UseMetrics()
                        .UseStartup<Startup>();
                });
    }
}
