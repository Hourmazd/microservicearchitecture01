<# Get current directory path #>
$src = (Get-Item -Path ".\" -Verbose).FullName;

$cdProjectDir = [string]::Format("cd /d {0}\{1}",$src, "source\services\IdentityServer\IdentityServer.API\");
$params=@("/C"; $cdProjectDir; " && dotnet watch run";)
Start-Process -Verb runas "cmd.exe" $params

$cdProjectDir = [string]::Format("cd /d {0}\{1}",$src, "source\services\product\Product.API\");
$params=@("/C"; $cdProjectDir; " && dotnet watch run"; )
Start-Process -Verb runas "cmd.exe" $params

$cdProjectDir = [string]::Format("cd /d {0}\{1}",$src, "source\services\warehousing\Warehousing.API\");
$params=@("/C"; $cdProjectDir; " && dotnet watch run"; )
Start-Process -Verb runas "cmd.exe" $params

$cdProjectDir = [string]::Format("cd /d {0}\{1}",$src, "source\services\ordering\Ordering.API\");
$params=@("/C"; $cdProjectDir; " && dotnet watch run"; )
Start-Process -Verb runas "cmd.exe" $params

$cdProjectDir = [string]::Format("cd /d {0}\{1}",$src, "source\ApiGateway\ApiGateway.Aggregator.API\");
$params=@("/C"; $cdProjectDir; " && dotnet watch run"; )
Start-Process -Verb runas "cmd.exe" $params

$cdProjectDir = [string]::Format("cd /d {0}\{1}",$src, "source\ApiGateway\ApiGateway.API\");
$params=@("/C"; $cdProjectDir; " && dotnet watch run"; )
Start-Process -Verb runas "cmd.exe" $params
